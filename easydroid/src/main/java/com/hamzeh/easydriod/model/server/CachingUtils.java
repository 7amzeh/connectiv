package com.hamzeh.easydriod.model.server;

import android.content.Context;
import android.graphics.Bitmap;
import android.text.format.DateUtils;
import android.util.Log;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.Date;

public class CachingUtils {
    public static final int MAX_SIZE_MB = 30;

    public static String getCachedResponse(String cachingDir, String fileName) {
        File cacheDir = new File(cachingDir);
        File file = new File(cacheDir.getPath() + File.separator + fileName);
        if (!file.exists()) {
            return null;
        }
        final String NEW_LINE = System.getProperty("line.separator");
        StringBuilder stringBuilder = new StringBuilder();
        try {
            FileReader fReader = new FileReader(file);
            BufferedReader bReader = new BufferedReader(fReader);
            String line;
            while ((line = bReader.readLine()) != null) {
                stringBuilder.append(line).append(NEW_LINE);
            }
            return stringBuilder.toString().trim();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void cacheResponseAsync(final String response, final String cachingDir, final String fileName) {
        if (response == null || response.trim().isEmpty()) {
            return;
        }
        new Thread(new Runnable() {
            @Override
            public void run() {
                File cacheDir = new File(cachingDir);
                File file = new File(cacheDir.getPath() + File.separator + fileName);
                try {
                    FileWriter writer = new FileWriter(file);
                    writer.write(response);
                    writer.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }


    // TODO: clean the following

    public static void cacheDataAsync(final Context context, final String data, final String fileName) {
        if (context == null || data == null || data.trim().isEmpty()) {
            return;
        }

        new Thread(new Runnable() {
            @Override
            public void run() {
                File cacheDir = context.getApplicationContext().getCacheDir();
                File file = new File(cacheDir.getPath() + File.separator + fileName);
                try {
                    FileWriter writer = new FileWriter(file);
                    writer.write(data);
                    writer.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    public static String getCachedData(Context context, String fileName) {
        if (context == null) {
            return null;
        }
        File cacheDir = context.getCacheDir();
        File file = new File(cacheDir.getPath() + File.separator + fileName);
        if (!file.exists()) {
            return null;
        }
        final String NEW_LINE = System.getProperty("line.separator");
        StringBuilder stringBuilder = new StringBuilder();
        try {
            FileReader fReader = new FileReader(file);
            BufferedReader bReader = new BufferedReader(fReader);
            String line;
            while ((line = bReader.readLine()) != null) {
                stringBuilder.append(line).append(NEW_LINE);
            }
            return stringBuilder.toString().trim();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void deleteCachedDataAsync(final Context context, final String fileName) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                File cacheDir = context.getApplicationContext().getCacheDir();
                File file = new File(cacheDir.getPath() + File.separator + fileName);
                if (file.exists()) {
                    file.delete();
                }
            }
        }).start();
    }

    public static File bitmapToFile(Context context, Bitmap bitmap, String fileName) {
        File tempFile = null;
        try {
            tempFile = File.createTempFile(fileName, ".png", context.getCacheDir());
            FileOutputStream fOut = new FileOutputStream(tempFile);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, fOut);
            fOut.flush();
            fOut.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return tempFile;
    }

    public static boolean isCached(Context context, String fileName) {
        File cacheDir = context.getCacheDir();
        File file = new File(cacheDir.getPath() + File.separator + fileName);
        return file.exists();
    }

    public static int clearCacheFolder(final File dir, final int numDays) {
        int sizeMb = getCacheSize(dir);
        if (sizeMb < MAX_SIZE_MB) {
            return 0;
        }
        int deletedFilesCount = 0;
        if (dir != null && dir.isDirectory()) {
            try {
                long now = new Date().getTime();
                for (File child : dir.listFiles()) {
                    if (child.isDirectory()) {
                        deletedFilesCount += clearCacheFolder(child, numDays);
                    }
                    if (!child.getName().endsWith(".dex")) {
                        if (child.lastModified() < now - numDays * DateUtils.DAY_IN_MILLIS) {
                            Log.i("cleaning", child.getName() + " cleaned!");
                            if (child.delete()) {
                                deletedFilesCount++;
                            }
                        }
                    }
                }
            } catch (Exception e) {
                Log.e("cleaning", String.format("Failed to clean the cache, error %s", e.getMessage()));
            }
        }
        Log.i("cleaning", "cleaned " + deletedFilesCount + " files successfully.");
        return deletedFilesCount;
    }

    public static int getCacheSize(final File dir) {
        int size = 0;
        if (dir != null && dir.isDirectory()) {
            try {
                for (File child : dir.listFiles()) {
                    if (child.isDirectory()) {
                        size += getCacheSize(child);
                    }
                    if (!child.getName().endsWith(".dex")) {
                        size += child.length();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return size / 1024 / 1024;
    }
}
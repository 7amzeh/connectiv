package com.hamzeh.easydriod.model.server;

import com.hamzeh.easydriod.controller.enums.RequestMode;
import com.hamzeh.easydriod.controller.interfaces.OnResponseReady;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class RequestManager {
    private Map<String, EasydroidHttp> mTasks = new LinkedHashMap<>();
    private String mCachingDir;

    public RequestManager() {
    }

    public RequestManager(String cachingDir) {
        setCachingDir(cachingDir);
    }

    public void send(EasydroidRequest request) {
        EasydroidHttp easydroidHttp = new EasydroidHttp(request);
        easydroidHttp.setCachingDir(mCachingDir);
        mTasks.put(request.requestHash(), easydroidHttp);
        easydroidHttp.send();
    }

    public void cancelTasks() {
        for (String key : mTasks.keySet()) {
            if (mTasks.get(key) != null) {
                mTasks.get(key).cancel();
            }
        }
        mTasks.clear();
    }

    public void cancelTasks(WebService webService) {
        for (String key : mTasks.keySet()) {
            EasydroidHttp easydroidHttp = mTasks.get(key);
            if (easydroidHttp != null && easydroidHttp.getRequest().getWebService().equals(webService)) {
                easydroidHttp.cancel();
            }
        }
    }

    public void setCachingDir(String cachingDir) {
        mCachingDir = cachingDir;
    }

    /*** Params Builder */

    public RequestManager.Builder builder() {
        return new RequestManager.Builder();
    }

    public class Builder {
        private EasydroidRequest mRequest = new EasydroidRequest();

        private Builder() {
        }

        public RequestManager.Builder webService(WebService webService) {
            mRequest.setWebService(webService);
            return this;
        }

        public RequestManager.Builder callback(OnResponseReady onResponseReady) {
            mRequest.setCallback(onResponseReady);
            return this;
        }

        public RequestManager.Builder jsonBody(String jsonBody) {
            mRequest.setJsonBody(jsonBody);
            return this;
        }

        public RequestManager.Builder jsonBody(JsonBody jsonBody) {
            mRequest.setJsonBody(jsonBody);
            return this;
        }

        public RequestManager.Builder addHeader(String key, String value) {
            if (value != null) {
                mRequest.addHeader(key, value);
            }
            return this;
        }

        public RequestManager.Builder addFile(String key, String value) {
            if (value != null) {
                mRequest.addFile(key, value);
            }
            return this;
        }

        public void addFiles(String key, String[] files) {
            mRequest.addFiles(key, files);
        }

        public void addFiles(String key, List<String> files) {
            mRequest.addFiles(key, files);
        }

        public RequestManager.Builder addStringArray(String key, String[] stringArr) {
            mRequest.addStringArray(key, stringArr);
            return this;
        }

        public RequestManager.Builder addStringArray(String key, List<String> stringList) {
            mRequest.addStringArray(key, stringList);
            return this;
        }

        public RequestManager.Builder values(String... values) {
            mRequest.setValues(values);
            return this;
        }

        public RequestManager.Builder requestMode(RequestMode requestMode) {
            mRequest.setRequestMode(requestMode);
            return this;
        }

        public void send() {
            mRequest.onPreSend();
            RequestManager.this.send(mRequest);
        }
    }
}
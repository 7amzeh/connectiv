package com.hamzeh.easydriod.model.server;

import com.hamzeh.easydriod.controller.enums.RequestMode;
import com.hamzeh.easydriod.controller.enums.ResponseMode;

import java.util.Map;

public class Response {
    public Object result;
    public WebService webService;
    public Map<String, String> params;
    public Map<String, String> headers;
    public ResponseMode responseMode;
    public RequestMode requestMode;
    public Object tag;
    public String json;
    public String liveHash;
    public String cacheHash;
    public int responseCode;
    public long fetchingTime;
    public long parsingTime;

    public Response(Object result) {
        this.result = result;
    }

    public Response(Object result, ResponseMode responseMode) {
        this.result = result;
        this.responseMode = responseMode;
    }

    public Response(Object result,
                    WebService webService,
                    Map<String, String> params,
                    Map<String, String> headers,
                    ResponseMode responseMode,
                    RequestMode requestMode,
                    Object tag,
                    String json,
                    String liveHash,
                    String cacheHash,
                    int responseCode,
                    long fetchingTime,
                    long parsingTime) {
        this.result = result;
        this.webService = webService;
        this.params = params;
        this.headers = headers;
        this.responseMode = responseMode;
        this.requestMode = requestMode;
        this.tag = tag;
        this.json = json;
        this.liveHash = liveHash;
        this.cacheHash = cacheHash;
        this.responseCode = responseCode;
        this.fetchingTime = fetchingTime;
        this.parsingTime = parsingTime;
    }

    public boolean isSuccess() {
        boolean isOk = responseCode >= 200 && responseCode < 300;
        return isOk && webService.getResponseClass().isInstance(result);
    }

    public boolean shouldCache() {
        return responseMode == ResponseMode.LIVE &&
                webService.getWebServiceType() == EasydroidHttp.WebserviceType.GET;
    }

    public boolean liveDataAvailable() {
        return liveHash != null;
    }

    public boolean cacheDataAvailable() {
        return liveHash != null;
    }

    public boolean shouldShowUiError() {
        boolean b0 = requestMode == RequestMode.LIVE_ONLY || requestMode == RequestMode.CACHE_THEN_LIVE;
        boolean b1 = responseMode == ResponseMode.LIVE && b0;
        boolean b2 = requestMode == RequestMode.CACHE_ONLY || requestMode == RequestMode.LIVE_THEN_CACHE;
        boolean b3 = responseMode == ResponseMode.CACHE && b2;

        return b1 || b3;
    }
}
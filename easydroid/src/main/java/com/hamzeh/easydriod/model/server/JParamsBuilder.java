package com.hamzeh.easydriod.model.server;

import com.google.gson.Gson;
import com.hamzeh.easydriod.controller.utils.StringUtils;

import java.util.LinkedHashMap;
import java.util.Map;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

public class JParamsBuilder {
    private Map<String, Object> mParams;

    public static JParamsBuilder newInstance() {
        return new JParamsBuilder();
    }

    public JParamsBuilder() {
        mParams = new LinkedHashMap<>();
    }

    public String build() {
        String key = StringUtils.toBase64(WebService.JWT_KEY);
        try {
            return Jwts.builder()
                    .setClaims(mParams)
                    .signWith(SignatureAlgorithm.HS256, key)
                    .compact();
        } catch (Exception e) {
            e.printStackTrace();
            return new Gson().toJson(mParams);
        }
    }

    public JParamsBuilder append(String key, Object value) {
        mParams.put(key, value);
        return this;
    }
}

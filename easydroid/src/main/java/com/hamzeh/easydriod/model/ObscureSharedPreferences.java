package com.hamzeh.easydriod.model;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Map;
import java.util.Set;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

public class ObscureSharedPreferences implements SharedPreferences {
    public static final String TAG = ObscureSharedPreferences.class.getSimpleName();

    private SharedPreferences delegate;
    private static ObscureSharedPreferences prefs;

    public ObscureSharedPreferences(Context context) {
        this.delegate = context.getApplicationContext().getSharedPreferences(
                context.getPackageName(), Context.MODE_PRIVATE);
    }

    public synchronized static ObscureSharedPreferences getPrefs(Context context) {
        if (prefs == null) {
            prefs = new ObscureSharedPreferences(context.getApplicationContext());
        }
        return prefs;
    }

    public class Editor implements SharedPreferences.Editor {
        SharedPreferences.Editor delegate;

        @SuppressLint("CommitPrefEdits")
        public Editor() {
            this.delegate = ObscureSharedPreferences.this.delegate.edit();
        }

        @Override
        public Editor putBoolean(String key, boolean value) {
            delegate.putString(key, encrypt(Boolean.toString(value)));
            return this;
        }

        @Override
        public Editor putFloat(String key, float value) {
            delegate.putString(key, encrypt(Float.toString(value)));
            return this;
        }

        @Override
        public Editor putInt(String key, int value) {
            delegate.putString(key, encrypt(Integer.toString(value)));
            return this;
        }

        @Override
        public Editor putLong(String key, long value) {
            delegate.putString(key, encrypt(Long.toString(value)));
            return this;
        }

        @Override
        public Editor putString(String key, String value) {
            delegate.putString(key, encrypt(value));
            return this;
        }

        @Override
        public void apply() {
            //to maintain compatibility with android level 7
            delegate.commit();
        }

        @Override
        public Editor clear() {
            delegate.clear();
            return this;
        }

        @Override
        public boolean commit() {
            return delegate.commit();
        }

        @Override
        public Editor remove(String s) {
            delegate.remove(s);
            return this;
        }

        @Override
        public SharedPreferences.Editor putStringSet(String key, Set<String> values) {
            throw new RuntimeException("This class does not work with String Sets.");
        }
    }

    public Editor edit() {
        return new Editor();
    }

    @Override
    public Map<String, ?> getAll() {
        throw new UnsupportedOperationException(); // left as an exercise to the reader
    }

    @Override
    public boolean getBoolean(String key, boolean defValue) {
        try {
            String v = delegate.getString(key, null);
            return v == null ? defValue : Boolean.parseBoolean(decrypt(v));
        } catch (Exception e) {
            return defValue;
        }
    }

    @Override
    public float getFloat(String key, float defValue) {
        try {
            String v = delegate.getString(key, null);
            return Float.parseFloat(decrypt(v));
        } catch (Exception e) {
            return defValue;
        }
    }

    @Override
    public int getInt(String key, int defValue) {
        try {
            String v = delegate.getString(key, null);
            return Integer.parseInt(decrypt(v));
        } catch (Exception e) {
            return defValue;
        }
    }

    @Override
    public long getLong(String key, long defValue) {
        try {
            String v = delegate.getString(key, null);
            return Long.parseLong(decrypt(v));
        } catch (Exception e) {
            return defValue;
        }
    }

    @Override
    public String getString(String key, String defValue) {
        try {
            String v = delegate.getString(key, null);
            return v != null ? decrypt(v) : defValue;
        } catch (Exception e) {
            return defValue;
        }
    }

    @Override
    public boolean contains(String s) {
        return delegate.contains(s);
    }

    @Override
    public void registerOnSharedPreferenceChangeListener(OnSharedPreferenceChangeListener onSharedPreferenceChangeListener) {
        delegate.registerOnSharedPreferenceChangeListener(onSharedPreferenceChangeListener);
    }

    @Override
    public void unregisterOnSharedPreferenceChangeListener(OnSharedPreferenceChangeListener onSharedPreferenceChangeListener) {
        delegate.unregisterOnSharedPreferenceChangeListener(onSharedPreferenceChangeListener);
    }

    @Override
    public Set<String> getStringSet(String key, Set<String> defValues) {
        throw new RuntimeException("This class does not work with String Sets.");
    }

    /*
     * Encryption / Decryption
     */

    private static final String PASSWORD = "Moi6kwmFV/PNpYLWzIS+vw==";
    private static final String SALT = "zxmonz3CLZxnimFvf+d+MjxJ8rjhCjodwcsOxPJZXEA=";
    private static final byte[] IV = {-89, -19, 18, -84, 86, 102, -31, 31, -6, -111, 61, -75, -85, 95, 120, -50};

    private static Encryption encryption;

    private synchronized Encryption getEncryption() {
        if (encryption == null) {
            encryption = Encryption.getDefault(PASSWORD, SALT, IV);
        }
        return encryption;
    }

    private String encrypt(String value) {
        try {
            return getEncryption().encrypt(value);
        } catch (Exception e) {
            Log.e(TAG, "error in decryption, possibly wrong key!");
            return null;
        }
    }

    private String decrypt(String value) throws Exception {
        return getEncryption().decrypt(value);
    }
}

package com.hamzeh.easydriod.model;

import com.hamzeh.easydriod.view.items.ListItem;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

public class ModelUtils {
    public static List<ListItem> getNoDuplicated(List<? extends ListItem> items) {
        Set<ListItem> set = new LinkedHashSet<>(items);
        return new ArrayList<>(set);
    }
}
package com.hamzeh.easydriod.model.server;

import com.hamzeh.easydriod.controller.enums.RequestMode;
import com.hamzeh.easydriod.controller.interfaces.OnResponseReady;
import com.hamzeh.easydriod.controller.utils.HashUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.HttpUrl;

public class EasydroidRequest {
    private RequestMode mRequestMode = RequestMode.LIVE_ONLY;
    private Map<String, String> mHeaders = new HashMap<>();
    private Map<String, String> mFiles = new HashMap<>();
    private Map<String, String[]> mFileArrays = new HashMap<>();
    private Map<String, String[]> mStringArrays = new HashMap<>();
    private String[] mValues = new String[]{};
    private String mJsonBody;

    private WebService mWebService;
    private OnResponseReady mOnResponseReady;

    private Map<String, String> mParamsMap;
    private Map<String, String> mJwtParamsMap;

    public void addHeader(String key, String value) {
        mHeaders.put(key, value);
    }

    public void setDefaultHeaders(Map<String, String> defaultHeaders) {
        if (mHeaders.isEmpty() && defaultHeaders != null) {
            mHeaders.putAll(defaultHeaders);
        }
    }

    public void setHeaders(Map<String, String> headers) {
        mHeaders = headers;
    }

    public Map<String, String> getHeaders() {
        return mHeaders;
    }

    public void addStringArray(String key, String[] stringArr) {
        mFileArrays.put(key, stringArr);
    }

    public void addStringArray(String key, List<String> stringList) {
        mFileArrays.put(key, stringList.toArray(new String[0]));
    }

    public void addFiles(String key, String[] files) {
        mFileArrays.put(key, files);
    }

    public void addFiles(String key, List<String> files) {
        mFileArrays.put(key, files.toArray(new String[0]));
    }

    public void addFile(String key, String value) {
        mFiles.put(key, value);
    }

    public Map<String, String> getFiles() {
        return mFiles;
    }

    public Map<String, String[]> getFileArrays() {
        return mFileArrays;
    }

    public boolean hasFiles() {
        return mFiles.size() + mFileArrays.size() > 0;
    }

    public Map<String, String[]> getStringArrays() {
        return mStringArrays;
    }

    public void setValues(String[] values) {
        mValues = values;
    }

    public String[] getValues() {
        return mValues;
    }

    public void setWebService(WebService webService) {
        mWebService = webService;
    }

    public WebService getWebService() {
        return mWebService;
    }

    public void setCallback(OnResponseReady onResponseReady) {
        mOnResponseReady = onResponseReady;
    }

    public OnResponseReady getCallback() {
        return mOnResponseReady;
    }

    public void setJsonBody(JsonBody jsonBody) {
        mJsonBody = jsonBody.toJson();
    }

    public void setJsonBody(String jsonBody) {
        mJsonBody = jsonBody;
    }

    public boolean hasJsonBody() {
        return mJsonBody != null;
    }

    public String getJsonBody() {
        return mJsonBody;
    }

    public String requestHash() {
        return HashUtils.calcHashSHA1(getWebService().getFullUrl(mValues));
    }

    public Map<String, String> getParamsMap() {
        return mParamsMap;
    }

    public Map<String, String> getJwtParamsMap() {
        return mJwtParamsMap;
    }

    public Map<String, String> getFinalParamsMap() {
        return mWebService.withJwt() ? mJwtParamsMap : mParamsMap;
    }

    public void setRequestMode(RequestMode requestMode) {
        mRequestMode = requestMode;
    }

    public RequestMode getRequestMode() {
        return mRequestMode;
    }

    public void onPreSend() {
        mParamsMap = mWebService.getParamsMap(mValues);
        mJwtParamsMap = mWebService.getJwtParamsMap(mValues);
    }

    public String getFullUrl() {
        try {
            HttpUrl httpUrl = HttpUrl.parse(getWebService().getUrlWithApi());
            if (httpUrl == null) return null;
            HttpUrl.Builder urlBuilder = httpUrl.newBuilder();
            for (Map.Entry<String, String> param : getFinalParamsMap().entrySet()) {
                urlBuilder.addQueryParameter(param.getKey(), param.getValue());
            }
            return urlBuilder.build().toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}

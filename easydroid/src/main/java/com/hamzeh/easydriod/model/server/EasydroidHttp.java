package com.hamzeh.easydriod.model.server;

import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.v4.util.ObjectsCompat;
import android.util.Log;

import com.hamzeh.easydriod.controller.enums.RequestMode;
import com.hamzeh.easydriod.controller.enums.ResponseMode;
import com.hamzeh.easydriod.controller.utils.HashUtils;
import com.hamzeh.easydriod.model.parsers.BaseParser;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.HttpsURLConnection;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;

public class EasydroidHttp {
    public enum WebserviceType {GET, POST, POST_MULTIPART, PUT, DELETE}

    private static final String TAG = EasydroidHttp.class.getSimpleName();
    private static final MediaType MEDIA_TYPE_JSON = MediaType.parse("application/json; charset=utf-8");

    private static Map<String, String> mDefaultHeaders = new HashMap<>();

    private Call mCall;
    private EasydroidRequest mRequest;
    private String mCachingDir;
    private String mCachedHash;
    private String mLiveHash;

    private long mTimePreRequest;
    private int mTag;

    private boolean mIsCanceled;

    private static OkHttpClient mOkHttpClient;
    private static EasydroidInitializer mInitializer;

    public EasydroidHttp(EasydroidRequest request) {
        initOkHttpClient();
        mRequest = request;
        mRequest.setDefaultHeaders(mDefaultHeaders);
        Log.i(TAG, "Calling: " + mRequest.getWebService().getUrlWithApi());
    }

    private void initOkHttpClient() {
        if (mOkHttpClient != null) {
            return;
        }
        if (mInitializer != null) {
            mOkHttpClient = mInitializer.getOkHttpClient();
        }
        if (mOkHttpClient == null) {
            OkHttpClient.Builder okHttpClientBuilder = new OkHttpClient.Builder();
            mOkHttpClient = okHttpClientBuilder.connectTimeout(10, TimeUnit.SECONDS)
                    .writeTimeout(10, TimeUnit.SECONDS)
                    .readTimeout(30, TimeUnit.SECONDS)
                    .build();
        }
        refreshDefaultHeaders();
        refreshJwtToken();
    }

    public static void refreshDefaultHeaders() {
        if (mInitializer != null) {
            mDefaultHeaders = mInitializer.getDefaultHeaders();
        }
    }

    private void refreshJwtToken() {
        WebService.JWT_KEY = mInitializer.getJwtKey();
        WebService.JWT_PARAM_NAME = mInitializer.getJwtParamName();
    }

    public void send() {
        WebService webService = mRequest.getWebService();
        switch (webService.getWebServiceType()) {
            case POST: {
                if (mRequest.hasFiles()) {
                    callPostMultipart();
                } else {
                    call(WebserviceType.POST);
                }
                break;
            }
            case POST_MULTIPART: {
                callPostMultipart();
                break;
            }
            case PUT: {
                call(WebserviceType.PUT);
                break;
            }
            case DELETE: {
                call(WebserviceType.DELETE);
                break;
            }
            case GET: {
                callGet();
                break;
            }
        }
    }

    private void callGet() {
        String fullUrl = mRequest.getFullUrl();
        if (fullUrl == null) {
            handleFailure();
            return;
        }
        if (mRequest.getRequestMode().shouldGetCacheInitially()) {
            fetchCacheAsync();
            if (mRequest.getRequestMode() == RequestMode.CACHE_ONLY) {
                return;
            }
        }
        Request.Builder requestBuilder = new Request.Builder();
        requestBuilder.url(fullUrl);
        for (Map.Entry<String, String> header : mRequest.getHeaders().entrySet()) {
            requestBuilder.addHeader(header.getKey(), header.getValue());
        }
        Request request = requestBuilder.build();
        mCall = mOkHttpClient.newCall(request);
        mCall.enqueue(new Callback() {
            @Override
            public void onResponse(@NonNull Call call, @NonNull Response response) {
                handleResponse(response);
            }

            @Override
            public void onFailure(@NonNull Call call, @NonNull IOException e) {
                e.printStackTrace();
                handleFailure();
                if (mRequest.getRequestMode().shouldGetCacheFinally()) {
                    fetchCacheAsync();
                }
            }
        });
    }

    private void call(WebserviceType webserviceType) {
        if (mRequest.hasJsonBody()) {
            RequestBody requestBody = RequestBody.create(MEDIA_TYPE_JSON, mRequest.getJsonBody());
            request(webserviceType, requestBody);
            return;
        }

        FormBody.Builder builder = new FormBody.Builder();
        for (Map.Entry<String, String> keyValue : mRequest.getFinalParamsMap().entrySet()) {
            builder.add(keyValue.getKey(), keyValue.getValue());
        }
        for (Map.Entry<String, String[]> keyStringArray : mRequest.getStringArrays().entrySet()) {
            for (String eachString : keyStringArray.getValue()) {
                builder.add(keyStringArray.getKey(), eachString);
            }
        }
        request(webserviceType, builder.build());
    }

    private void callPostMultipart() {
        if (mRequest.hasJsonBody()) {
            RequestBody requestBody = RequestBody.create(MEDIA_TYPE_JSON, mRequest.getJsonBody());
            request(WebserviceType.POST_MULTIPART, requestBody);
            return;
        }

        MultipartBody.Builder builder = new MultipartBody.Builder();
        builder.setType(MultipartBody.FORM);
        for (Map.Entry<String, String> keyValue : mRequest.getFinalParamsMap().entrySet()) {
            builder.addFormDataPart(keyValue.getKey(), keyValue.getValue());
        }
        for (Map.Entry<String, String[]> keyStringArray : mRequest.getStringArrays().entrySet()) {
            for (String eachString : keyStringArray.getValue()) {
                builder.addFormDataPart(keyStringArray.getKey(), eachString);
            }
        }
        for (Map.Entry<String, String> keyValue : mRequest.getFiles().entrySet()) {
            File file = new File(keyValue.getValue());
            builder.addFormDataPart(keyValue.getKey(), file.getName(),
                    RequestBody.create(MediaType.parse("image/jpeg"), file));
        }
        for (Map.Entry<String, String[]> keyStringArray : mRequest.getFileArrays().entrySet()) {
            for (String eachString : keyStringArray.getValue()) {
                File file = new File(eachString);
                builder.addFormDataPart(keyStringArray.getKey(), file.getName(),
                        RequestBody.create(MediaType.parse("image/jpeg"), file));
            }
        }
        request(WebserviceType.POST_MULTIPART, builder.build());
    }

    private void request(WebserviceType webserviceType, RequestBody requestBody) {
        Request.Builder requestBuilder = new Request.Builder();
        String url = mRequest.getWebService().getUrlWithApi();

        switch (webserviceType) {
            case POST:
            case POST_MULTIPART: {
                requestBuilder.url(url).post(requestBody);
                break;
            }
            case PUT: {
                requestBuilder.url(url).put(requestBody);
                break;
            }
            case DELETE: {
                requestBuilder.url(mRequest.getFullUrl()).delete();
                break;
            }
            default: {
                throw new RuntimeException("webserviceType must be either POST, PUT, or DELETE");
            }
        }

        for (Map.Entry<String, String> header : mRequest.getHeaders().entrySet()) {
            requestBuilder.addHeader(header.getKey(), header.getValue());
        }
        Request request = requestBuilder.build();
        mTimePreRequest = System.currentTimeMillis();
        mCall = mOkHttpClient.newCall(request);
        mCall.enqueue(new Callback() {
            @Override
            public void onResponse(@NonNull Call call, @NonNull Response response) {
                handleResponse(response);
            }

            @Override
            public void onFailure(@NonNull Call call, @NonNull IOException e) {
                e.printStackTrace();
                handleFailure();
            }
        });
    }

    private void handleResponse(Response response) {
        long fetchingTime = System.currentTimeMillis() - mTimePreRequest;
        String json = extractJson(response);
        mLiveHash = HashUtils.calcHashSHA1(json);
        long timePreParsing = System.currentTimeMillis();
        Object liveResult = parse(json);
        long parsingTime = System.currentTimeMillis() - timePreParsing;
        done(liveResult, ResponseMode.LIVE, json, response.code(), fetchingTime, parsingTime);
    }

    private void handleFailure() {
        long requestTime = System.currentTimeMillis() - mTimePreRequest;
        done(null, ResponseMode.LIVE, null, 0, requestTime, 0);
    }

    private String extractJson(Response response) {
        ResponseBody responseBody = response.body();
        try {
            if (responseBody != null) {
                return responseBody.string();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public Object parse(String json) {
        try {
            BaseParser parser = ((BaseParser) mRequest.getWebService().getParserClass().newInstance());
            return parser.parse(mRequest.getWebService().getResponseClass(), json);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private void done(Object result,
                      ResponseMode responseMode,
                      String json,
                      int responseCode,
                      long fetchingTime,
                      long parsingTime) {
        com.hamzeh.easydriod.model.server.Response response = new com.hamzeh.easydriod.model.server.Response(result,
                mRequest.getWebService(),
                mRequest.getParamsMap(),
                mRequest.getHeaders(),
                responseMode,
                mRequest.getRequestMode(),
                mTag,
                json,
                mCachedHash,
                mCachedHash,
                responseCode,
                fetchingTime,
                parsingTime);
        done(response);
    }

    private void done(final com.hamzeh.easydriod.model.server.Response response) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                if (mRequest.getCallback() == null || isCancelled()) {
                    return;
                }
                if (response.isSuccess()) {
                    mRequest.getCallback().onSuccess(response);
                    cacheResponseAsync(response);
                } else {
                    mRequest.getCallback().onFailure(response);
                }
            }
        });
    }

    private void cacheResponseAsync(com.hamzeh.easydriod.model.server.Response response) {
        if (response.shouldCache()) {
            CachingUtils.cacheResponseAsync(response.json, mCachingDir, mRequest.requestHash());
        }
    }

    public static void setDefaultHeaders(Map<String, String> headers) {
        mDefaultHeaders = headers;
    }

    public void cancel() {
        mIsCanceled = true;
        if (mCall != null) {
            mCall.cancel();
        }
    }

    public void setCachingDir(String cachingDir) {
        mCachingDir = cachingDir;
    }

    public boolean isCancelled() {
        return mIsCanceled || mCall != null && mCall.isCanceled();
    }

    public EasydroidRequest getRequest() {
        return mRequest;
    }

    /*** Caching */

    private void fetchCacheAsync() {
        if (isCancelled() || mCachingDir == null || mRequest.getRequestMode() == RequestMode.LIVE_ONLY) {
            return;
        }
        new Thread(new Runnable() {
            @Override
            public void run() {
                fetchCache();
            }
        }).start();
    }

    private void fetchCache() {
        long timePreStorageRead = System.currentTimeMillis();
        String json = CachingUtils.getCachedResponse(mCachingDir, mRequest.requestHash());
        mCachedHash = HashUtils.calcHashSHA1(json);
        long fetchingTime = System.currentTimeMillis() - timePreStorageRead;
        long timePreParsing = System.currentTimeMillis();
        Object result = parse(json);
        long parsingTime = System.currentTimeMillis() - timePreParsing;
        if (result != null || mRequest.getRequestMode() == RequestMode.CACHE_ONLY) {
            done(result, ResponseMode.CACHE, json, HttpsURLConnection.HTTP_OK, fetchingTime, parsingTime);
        }
    }

    public boolean isUpdated() {
        return !ObjectsCompat.equals(mLiveHash, mCachedHash);
    }

    public static void initWith(EasydroidInitializer initializer) {
        mInitializer = initializer;
    }

    public interface EasydroidInitializer {
        Map<String, String> getDefaultHeaders();

        OkHttpClient getOkHttpClient();

        String getJwtKey();

        String getJwtParamName();
    }
}
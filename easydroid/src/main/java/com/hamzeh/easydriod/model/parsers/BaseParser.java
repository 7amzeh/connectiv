package com.hamzeh.easydriod.model.parsers;

public abstract class BaseParser {
    public abstract Object parse(Class responseClass, String json);
}
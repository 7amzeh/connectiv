package com.hamzeh.easydriod.model;

import android.support.v4.app.Fragment;

public class PageEntity {
    public Fragment fragment;
    public String title;

    public PageEntity(Fragment fragment, String title) {
        this.fragment = fragment;
        this.title = title;
    }
}
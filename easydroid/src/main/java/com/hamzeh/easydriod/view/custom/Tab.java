package com.hamzeh.easydriod.view.custom;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hamzeh.easydriod.view.custom.CheckableImageView;

import org.hamzeh.easydroid.R;

public class Tab extends LinearLayout {
    private CheckableImageView iconImageView;
    private TextView titleTextView;
    private boolean mIsChecked;
    private int mActiveColorResId;

    public Tab(Context context) {
        super(context);
        init(null);
    }

    public Tab(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public Tab(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }

    public void set(int iconResId, int titleResId, boolean isChecked, int activeColorResId) {
        iconImageView.setImageResource(iconResId);
        iconImageView.setChecked(isChecked);
        titleTextView.setText(titleResId);
        mActiveColorResId = activeColorResId;
        setActive(isChecked);
    }

    private void init(AttributeSet attrs) {
        inflate(getContext(), R.layout.custom_tab, this);
        iconImageView = findViewById(R.id.img_icon);
        titleTextView = findViewById(R.id.txt_title);
        if (attrs != null) {
            TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.Tab);
            int iconResId = typedArray.getResourceId(R.styleable.Tab_custom_icon, 0);
            iconImageView.setImageResource(iconResId);
            int titleResId = typedArray.getResourceId(R.styleable.Tab_custom_title, R.string.app_name);
            titleTextView.setText(titleResId);
            mIsChecked = typedArray.getBoolean(R.styleable.Tab_isChecked, false);
            setActive(mIsChecked);
            typedArray.recycle();
        }
    }

    public void setActive(boolean isChecked) {
        mIsChecked = isChecked;
        int colorResId = isChecked ? mActiveColorResId : R.color.white_transparent;
        int color = ContextCompat.getColor(getContext(), colorResId);
        titleTextView.setTextColor(color);
        iconImageView.setChecked(isChecked);
    }

    public void padImage() {
        int imageBottomPadding = (int) getResources().getDimension(R.dimen.margin4);
        iconImageView.setPadding(0, 0, 0, imageBottomPadding);
    }

    public String getTitle() {
        return titleTextView.getText().toString();
    }
}
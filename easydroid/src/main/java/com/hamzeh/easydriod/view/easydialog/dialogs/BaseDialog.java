package com.hamzeh.easydriod.view.easydialog.dialogs;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;

import org.hamzeh.easydroid.R;

public abstract class BaseDialog extends AlertDialog implements DialogInterface.OnShowListener,
        Animation.AnimationListener, DialogInterface.OnDismissListener {
    public static final float DEFAULT_X_FACTOR = 0.93f;
    public static final int STATUS_READY = 0;
    public static final int STATUS_EXPANDING = 1;
    public static final int STATUS_EXPANDED = 2;

    protected View dialogView;
    protected View containerView;
    protected Rect mRect;
    protected Object mTag;
    protected View.OnClickListener mOnClickListener;
    protected int animationStatus = STATUS_READY;
    protected int scWidth, scHeight;
    private boolean cancelableWhenTouchOutside = true;

    public BaseDialog(Context context, View.OnClickListener onClickListener) {
        super(context, R.style.TransDialog);
        mOnClickListener = onClickListener;
        setupView();
    }

    protected void setupView() {
        DisplayMetrics dm = getContext().getResources().getDisplayMetrics();
        scWidth = dm.widthPixels;
        scHeight = dm.heightPixels;
        LayoutInflater inflater = LayoutInflater.from(getContext());
        dialogView = inflater.inflate(getLayoutResId(), null, false);
        prepareContainerView();
    }

    protected abstract int getLayoutResId();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setView(dialogView);
        setOnShowListener(this);
        setOnDismissListener(this);
        if (getWindow() != null) {
            getWindow().getAttributes().windowAnimations = getWindowAnimation();
        }
        super.onCreate(savedInstanceState);
    }

    protected int getWindowAnimation() {
        return R.style.DialogAnimationVertical;
    }

    protected void prepareContainerView() {
        containerView = dialogView.findViewById(R.id.container);
        if (containerView == null) return;
        containerView.getLayoutParams().width = (int) (scWidth * getXFactor());
        containerView.getLayoutParams().height = scHeight;
        containerView.requestLayout();
        allowCancelOnTouchOutside(true);
    }

    protected void allowCancelOnTouchOutside(boolean allow) {
        if (allow) {
            containerView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (animationStatus == STATUS_EXPANDED && cancelableWhenTouchOutside) {
                        dismiss();
                    }
                }
            });
        } else {
            containerView.setClickable(false);
        }
    }

    protected float getXFactor() {
        return DEFAULT_X_FACTOR;
    }

    @Override
    public void setCanceledOnTouchOutside(boolean cancel) {
        cancelableWhenTouchOutside = cancel;
        allowCancelOnTouchOutside(cancel);
        super.setCanceledOnTouchOutside(cancel);
    }

    @Override
    public void setCancelable(boolean flag) {
        setCanceledOnTouchOutside(flag);
        super.setCancelable(flag);
    }

    @Override
    public void onShow(DialogInterface dialog) {
        if (mRect != null) {
            animateDelayed();
        } else {
            if (containerView != null) {
                containerView.setVisibility(View.VISIBLE);
            }
            animationStatus = BaseDialog.STATUS_EXPANDED;
        }
    }

    private void animateDelayed() {
        containerView.postDelayed(new Runnable() {
            @Override
            public void run() {
                animate(BaseDialog.this, 0, 1, 350);
            }
        }, 100);
    }

    private void animate(Animation.AnimationListener listener, float from, float to, int duration) {
        if (mRect == null) {
            BaseDialog.super.dismiss();
            return;
        }
        float pivotX = (float) mRect.centerX() / scWidth;
        float pivotY = (float) mRect.centerY() / scHeight;
        ScaleAnimation grow = new ScaleAnimation(from, to, from, to,
                Animation.RELATIVE_TO_SELF, pivotX, Animation.RELATIVE_TO_SELF, pivotY);
        grow.setDuration(duration);
        grow.setFillAfter(true);
        grow.setAnimationListener(listener);
        containerView.clearAnimation();
        containerView.startAnimation(grow);
    }

    public void setRect(View view) {
        Rect rect = new Rect();
        view.getGlobalVisibleRect(rect);
        setRect(rect);
    }

    public void setRect(Rect rect) {
        mRect = rect;
    }

    public void setTag(Object tag) {
        this.mTag = tag;
    }

    public Object getTag() {
        return mTag;
    }

    @Override
    public void onAnimationStart(Animation animation) {
        animationStatus = STATUS_EXPANDING;
        containerView.postDelayed(new Runnable() {
            @Override
            public void run() {
                containerView.setVisibility(View.VISIBLE);
            }
        }, 100);
    }

    @Override
    public void onAnimationEnd(Animation animation) {
        animationStatus = STATUS_EXPANDED;
    }

    @Override
    public void onAnimationRepeat(Animation animation) {
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        if (containerView != null) {
            containerView.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void dismiss() {
        animate(collapseAnimationListener, 1, 0, 200);
    }

    @Override
    public void cancel() {
        animate(collapseAnimationListener, 1, 0, 200);
    }

    private Animation.AnimationListener collapseAnimationListener = new Animation.AnimationListener() {
        @Override
        public void onAnimationStart(Animation animation) {
        }

        @Override
        public void onAnimationEnd(Animation animation) {
            BaseDialog.super.dismiss();
        }

        @Override
        public void onAnimationRepeat(Animation animation) {
        }
    };

    public interface OnClickListener {
        void onDialogPositive();

        void onDialogNegative();
    }
}
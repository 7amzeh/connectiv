package com.hamzeh.easydriod.view.viewholders;

import android.view.View;

import com.hamzeh.easydriod.controller.interfaces.ListItemCallback;

public class BlankViewHolder extends EasydroidViewHolder {
    public BlankViewHolder(View itemView) {
        super(itemView);
    }

    public BlankViewHolder(View itemView, ListItemCallback callback) {
        super(itemView, callback);
    }
}
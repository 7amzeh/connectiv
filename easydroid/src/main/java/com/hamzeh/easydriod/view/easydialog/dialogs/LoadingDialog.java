package com.hamzeh.easydriod.view.easydialog.dialogs;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;

import org.hamzeh.easydroid.R;

public class LoadingDialog extends AlertDialog {
    private Context mContext;

    public LoadingDialog(Context context) {
        super(context, R.style.TransDialog);
        mContext = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View dialogView = inflater.inflate(R.layout.dialog_loading, null, false);
        setView(dialogView);
        setCanceledOnTouchOutside(false);
        super.onCreate(savedInstanceState);
        initWindow();
    }

    private void initWindow() {
        getWindow().setGravity(Gravity.CENTER);
        getWindow().setDimAmount(0.5f);
        getWindow().getAttributes().windowAnimations = R.style.DialogNoAnimation;
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    }
}
package com.hamzeh.easydriod.view.items;

import com.hamzeh.easydriod.controller.interfaces.LoadMore;
import com.hamzeh.easydriod.controller.interfaces.Releasable;
import com.hamzeh.easydriod.view.viewholders.LoadingMoreViewHolder;

import org.hamzeh.easydroid.R;

public class LoadingMore extends ListItem {
    public static ListItemType LOADING_MORE = new ListItemType(LoadingMoreViewHolder.class, R.layout.item_loading_more);
    public static ListItemType LOADING_MORE_SMALL = new ListItemType(LoadingMoreViewHolder.class, R.layout.item_loading_more_small);

    private LoadMore mLoadMore;
    private Releasable mReleasable;
    private int code = 555;

    public LoadingMore(LoadMore loadMore) {
        this();
        mLoadMore = loadMore;
    }

    public LoadingMore() {
        mListItemType = LOADING_MORE;
    }

    @Override
    public ListItemType getListItemType() {
        return mListItemType;
    }

    public void loadMore() {
        if (mLoadMore != null) {
            mLoadMore.loadMore();
        }
    }

    public void setReleasable(Releasable releasable) {
        mReleasable = releasable;
    }

    public void release() {
        if (mReleasable != null) {
            mReleasable.release();
        }
    }

    @Override
    public int hashCode() {
        return code;
    }

    @Override
    public boolean equals(Object other) {
        return other == this || other instanceof LoadingMore;
    }
}
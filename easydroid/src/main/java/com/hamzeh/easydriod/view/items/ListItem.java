package com.hamzeh.easydriod.view.items;

public class ListItem {
    protected transient ListItemType mListItemType;
    protected transient boolean mSelected;

    public long getStableId() {
        return 0;
    }

    public ListItemType getListItemType() {
        return mListItemType;
    }

    public void setListItemType(ListItemType listItemType) {
        this.mListItemType = listItemType;
    }

    public boolean isSelected() {
        return mSelected;
    }

    public void setSelected(boolean selected) {
        mSelected = selected;
    }
}
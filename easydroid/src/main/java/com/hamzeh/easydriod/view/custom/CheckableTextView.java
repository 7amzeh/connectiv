package com.hamzeh.easydriod.view.custom;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.widget.Checkable;

import com.hamzeh.easydriod.view.FontUtils;

import org.hamzeh.easydroid.R;

public class CheckableTextView extends CustomTextView implements Checkable {
    protected boolean isChecked;
    protected int mCheckedTextColor;
    protected int mTextColor;

    private static final int[] CHECKED_STATE_SET = {android.R.attr.state_checked};

    public CheckableTextView(final Context context, final AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    @Override
    protected void init(AttributeSet attrs) {
        super.init(attrs);
        mTextColor = getCurrentTextColor();
        if (attrs != null) {
            TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.CheckableTextView);
            int mCheckedTextColorResId = typedArray.getResourceId(R.styleable.CheckableTextView_checked_text_color, android.R.color.white);
            mCheckedTextColor = ContextCompat.getColor(getContext(), mCheckedTextColorResId);
            typedArray.recycle();
        }
    }

    @Override
    public int[] onCreateDrawableState(final int extraSpace) {
        final int[] drawableState = super.onCreateDrawableState(extraSpace + 1);
        if (isChecked()) {
            mergeDrawableStates(drawableState, CHECKED_STATE_SET);
        }
        return drawableState;
    }

    @Override
    public void toggle() {
        setChecked(!isChecked);
    }

    @Override
    public boolean isChecked() {
        return isChecked;
    }

    @Override
    public void setChecked(boolean checked) {
        if (isChecked == checked) {
            return;
        }
        isChecked = checked;
        refreshDrawableState();
        setTextColor(isChecked ? mCheckedTextColor : mTextColor);
    }
}
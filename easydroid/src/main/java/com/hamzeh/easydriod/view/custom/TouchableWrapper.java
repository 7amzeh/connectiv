package com.hamzeh.easydriod.view.custom;

import android.content.Context;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.widget.FrameLayout;

public class TouchableWrapper extends FrameLayout implements Runnable {
    public static String TAG = TouchableWrapper.class.getSimpleName();
    public static boolean sMapIsTouched;
    public static int TIME_TO_RESET = 3000;

    private Handler handler = new Handler();

    public TouchableWrapper(Context context) {
        super(context);
    }

    public TouchableWrapper(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public TouchableWrapper(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        switch (ev.getAction()) {
            case MotionEvent.ACTION_DOWN:
                Log.i(TAG, "ACTION_DOWN");
                sMapIsTouched = true;
                handler.removeCallbacks(this);
                break;

            case MotionEvent.ACTION_UP:
                Log.i(TAG, "ACTION_UP");
                handler.postDelayed(this, TIME_TO_RESET);
                break;
        }

        return super.dispatchTouchEvent(ev);
    }

    public void turnOnTemp() {
        Log.i(TAG, "turnOnTemp");
        sMapIsTouched = true;
        handler.removeCallbacks(this);
        handler.postDelayed(this, TIME_TO_RESET);
    }

    public void onDestroyView() {
        Log.i(TAG, "onDestroyView");
        sMapIsTouched = false;
        handler.removeCallbacks(this);
    }

    @Override
    public void run() {
        Log.i(TAG, "run");
        sMapIsTouched = false;
    }
}
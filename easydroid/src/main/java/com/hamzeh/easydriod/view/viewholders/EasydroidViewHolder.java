package com.hamzeh.easydriod.view.viewholders;

import android.content.Context;
import android.support.annotation.IdRes;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.hamzeh.easydriod.controller.interfaces.ListItemCallback;
import com.hamzeh.easydriod.view.items.ListItem;

public class EasydroidViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    protected ListItemCallback mCallback;
    protected ListItem mListItem;
    protected int mPosition;

    public EasydroidViewHolder(View itemView) {
        super(itemView);
    }

    public EasydroidViewHolder(View itemView, ListItemCallback callback) {
        super(itemView);
        mCallback = callback;
        mPosition = getAdapterPosition();
    }

    public void draw(ListItem listItem) {
        mListItem = listItem;
        mPosition = getAdapterPosition();
    }

    protected void attachClickListener(View... views) {
        for (View view : views) {
            view.setOnClickListener(this);
        }
    }

    @SuppressWarnings("unchecked")
    protected <T extends View> T findViewById(@IdRes int id) {
        return (T) itemView.findViewById(id);
    }

    protected Context getContext() {
        return itemView.getContext();
    }

    protected String getString(int resId) {
        return getContext().getString(resId);
    }

    protected int getColor(int resId) {
        return ContextCompat.getColor(getContext(), resId);
    }

    protected String adapt(String text) {
        return text == null ? "" : text;
    }

    /*** Special Adapter Callbacks */

    public void onViewAttachedToWindow() {
    }

    public void onViewRecycled() {
    }

    public void onViewDetachedFromWindow() {
    }

    @Override
    public void onClick(View view) {
        if (mCallback != null) {
            mCallback.onItemClicked(view, mListItem, mPosition);
        }
    }
}
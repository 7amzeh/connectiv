package com.hamzeh.easydriod.view.items;

import android.util.SparseArray;

public class ItemTypesPool {
    public static final String TAG = ItemTypesPool.class.getSimpleName();

    private static SparseArray<ListItemType> sTypeItemMap = new SparseArray<>();

    public static ListItemType getItemType(int type) {
        return sTypeItemMap.get(type);
    }

    public static void put(int itemViewType, ListItemType listItemType) {
        sTypeItemMap.put(itemViewType, listItemType);
    }
}
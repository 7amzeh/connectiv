package com.hamzeh.easydriod.view.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;

import com.hamzeh.easydriod.controller.interfaces.ListItemCallback;
import com.hamzeh.easydriod.view.items.ItemTypesPool;
import com.hamzeh.easydriod.view.items.ListItem;
import com.hamzeh.easydriod.view.items.ListItemType;
import com.hamzeh.easydriod.view.viewholders.EasydroidViewHolder;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.reflect.Constructor;
import java.util.List;

public class EasydroidAdapter extends RecyclerArrayAdapter<ListItem, EasydroidViewHolder> {
    public static final String TAG = EasydroidAdapter.class.getSimpleName();

    public EasydroidAdapter(Context ctx, List<? extends ListItem> data, ListItemCallback callback) {
        super(ctx, callback);
        addAll(data);
    }

    @NonNull
    @Override
    public EasydroidViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ListItemType listItemType = ItemTypesPool.getItemType(viewType);
        View view = mInflater.inflate(listItemType.layoutResId, parent, false);
        try {
            Constructor constructors[] = listItemType.viewHolderClass.getDeclaredConstructors();
            if (constructors.length > 0) {
                if (constructors[0].getParameterTypes().length == 1) {
                    return (EasydroidViewHolder) constructors[0].newInstance(view);
                } else {
                    return (EasydroidViewHolder) constructors[0].newInstance(view, mListItemCallback);
                }
            }
        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            e.printStackTrace(pw);
            Log.e(TAG, sw.toString());
        }
        return new EasydroidViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull EasydroidViewHolder viewHolder, int position) {
        viewHolder.draw(mItems.get(position));
    }

    @Override
    public void onViewRecycled(@NonNull EasydroidViewHolder holder) {
        super.onViewRecycled(holder);
        holder.onViewRecycled();
    }

    @Override
    public void onViewDetachedFromWindow(@NonNull EasydroidViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        holder.onViewDetachedFromWindow();
    }

    @Override
    public void onViewAttachedToWindow(@NonNull EasydroidViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        holder.onViewAttachedToWindow();
    }
}
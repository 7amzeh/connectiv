package com.hamzeh.easydriod.view;

import android.widget.EditText;

import com.hamzeh.easydriod.view.items.ListItem;
import com.hamzeh.easydriod.view.items.ListItemType;

import com.hamzeh.easydriod.controller.interfaces.TextEnteredListener;

public class SearchItem extends ListItem {
    public EditText searchEditText;
    public TextEnteredListener textEnteredListener;
    public boolean clear;
    public boolean focused;
    public String searchText;

    public SearchItem() {
    }

    public SearchItem(TextEnteredListener textEnteredListener, ListItemType listItemType) {
        this.textEnteredListener = textEnteredListener;
        setListItemType(listItemType);
    }

    @Override
    public int hashCode() {
        return 555;
    }

    @Override
    public boolean equals(Object other) {
        return (other == this || other instanceof SearchItem);
    }
}

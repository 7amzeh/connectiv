package com.hamzeh.easydriod.view;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.hamzeh.easydriod.controller.utils.AppUtils;
import com.hamzeh.easydriod.view.custom.Loading;

import org.hamzeh.easydroid.R;

public class LoadingUtils extends RelativeLayout {
    public enum State {
        LOADING, NO_DATA, NO_CONNECTION, ERROR, DONE
    }

    private Loading mLoading;
    private TextView titleTextView;
    private ImageView iconImageView;
    private View messageView;
    private State mState = State.DONE;
    private OnReloadListener mOnReloadListener;
    private ViewVisibilityHandler messageViewHandler;
    private ViewVisibilityHandler loadingViewHandler;

    public int connectionErrorImageResId = R.drawable.ic_no_connection;
    public int noDataImageResId = R.drawable.ic_empty;
    public int unknownErrorImageResId = R.drawable.ic_error;

    public int connectionErrorTitleResId = R.string.error_no_internet_connection;
    public int noDataTitleResId = R.string.error_no_data;
    public int unknownErrorTitleResId = R.string.error_unknown_error;

    public LoadingUtils(Context context) {
        super(context);
        init();
    }

    public LoadingUtils(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public LoadingUtils(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        inflate(getContext(), R.layout.custom_loading_utils, this);
        messageView = findViewById(R.id.layout_message);
        prepareMessage();
        prepareLoading();
        messageViewHandler = new ViewVisibilityHandler(messageView, "message");
        loadingViewHandler = new ViewVisibilityHandler(mLoading, "loading");
    }

    public void removeLoading() {
        mLoading.stopSpinning();
        mLoading.setVisibility(View.GONE);
        ViewGroup vg = (ViewGroup) mLoading.getParent();
        vg.removeView(mLoading);
    }

    private void prepareMessage() {
        titleTextView = findViewById(R.id.txt_title);
        iconImageView = findViewById(R.id.img_icon);
    }

    private void prepareLoading() {
        mLoading = findViewById(R.id.loading);
    }

    public void setOnReloadListener(OnReloadListener onReloadListener) {
        mOnReloadListener = onReloadListener;
    }

    public State getState() {
        return mState;
    }

    public void setState(State state, int titleResId) {
        setState(state);
        titleTextView.setText(titleResId);
    }

    public void setTitle(int titleResId) {
        titleTextView.setText(titleResId);
    }

    public void setState(State state) {
        if (mState == state) {
            return;
        }
        mState = state;
        switch (mState) {
            case LOADING: {
                mLoading.spin();
                messageViewHandler.hide(R.anim.view_leave);
                loadingViewHandler.show(R.anim.view_enter);
                setOnClickListener(null);
                break;
            }
            case NO_DATA: {
                mLoading.stopSpinning();
                set(noDataImageResId, noDataTitleResId);
                messageViewHandler.show(R.anim.view_enter);
                loadingViewHandler.hide(R.anim.view_leave);
                setOnClickListener(onRootClickListener);
                break;
            }
            case NO_CONNECTION: {
                mLoading.stopSpinning();
                set(connectionErrorImageResId, connectionErrorTitleResId);
                messageViewHandler.show(R.anim.view_enter);
                loadingViewHandler.hide(R.anim.view_leave);
                setOnClickListener(onRootClickListener);
                break;
            }
            case ERROR: {
                mLoading.stopSpinning();
                set(unknownErrorImageResId, unknownErrorTitleResId);
                messageViewHandler.show(R.anim.view_enter);
                loadingViewHandler.hide(R.anim.view_leave);
                setOnClickListener(onRootClickListener);
                break;
            }
            case DONE: {
                mLoading.stopSpinning();
                messageViewHandler.hide(R.anim.view_leave);
                loadingViewHandler.hide(R.anim.view_leave);
                setOnClickListener(null);
                break;
            }
        }
    }

    @Override
    public boolean performClick() {
        return super.performClick();
    }

    @Override
    public boolean callOnClick() {
        return super.callOnClick();
    }

    private OnClickListener onRootClickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            if (mOnReloadListener != null) {
                if (mState == State.NO_CONNECTION && !AppUtils.isNetworkConnected(getContext())) {
                    blinkMessage();
                    return;
                }
                setState(State.LOADING);
                mOnReloadListener.onReload();
            }
        }
    };

    private void blinkMessage() {
        Animation blink = AnimationUtils.loadAnimation(getContext(), R.anim.blink);
        messageView.clearAnimation();
        messageView.startAnimation(blink);
    }

    public void set(int iconResId, int titleResId) {
        iconImageView.setImageResource(iconResId);
        titleTextView.setText(titleResId);
    }

    public void setMessageVisibility(int visibility, int iconResId, int titleResId) {
        iconImageView.setImageResource(iconResId);
        titleTextView.setText(titleResId);
        setMessageVisibility(visibility);
    }

    public void setMessageVisibility(int visibility) {
        messageView.setVisibility(visibility);
        if (visibility == View.VISIBLE) {
            mLoading.setVisibility(View.GONE);
        }
    }

    public interface OnReloadListener {
        void onReload();
    }
}
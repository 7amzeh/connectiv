package com.hamzeh.easydriod.view.viewholders;

import android.util.Log;
import android.view.View;

import com.hamzeh.easydriod.view.items.LoadingMore;
import com.hamzeh.easydriod.controller.interfaces.ListItemCallback;
import com.hamzeh.easydriod.controller.interfaces.Releasable;
import com.hamzeh.easydriod.view.custom.Loading;
import com.hamzeh.easydriod.view.items.ListItem;

import org.hamzeh.easydroid.R;

public class LoadingMoreViewHolder extends EasydroidViewHolder implements Releasable {
    private Loading mLoading;

    public LoadingMoreViewHolder(final View itemView, ListItemCallback callback) {
        super(itemView, callback);
        mLoading = findViewById(R.id.av_loading);
    }

    @Override
    public void draw(ListItem listItem) {
        super.draw(listItem);
        Log.i("LoadingMoreViewHolder", "draw");
        mLoading.setVisibility(View.VISIBLE);
        itemView.setVisibility(View.VISIBLE);
        LoadingMore loadingMore = (LoadingMore) listItem;
        loadingMore.setReleasable(this);
        loadingMore.loadMore();
        mLoading.spin();
    }

    @Override
    public void release() {
        mLoading.stopSpinning();
        mLoading.setVisibility(View.GONE);
        itemView.setVisibility(View.GONE);
    }
}
package com.hamzeh.easydriod.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.widget.Toast;

import org.hamzeh.easydroid.R;

public class EasydroidToast {
    private static Toast sToast;

    public static void showToast(Context ctx, int textResId) {
        showToast(ctx, ctx.getString(textResId));
    }

    public static void showToast(Context ctx, String text) {
        showToast(ctx, text, Toast.LENGTH_SHORT);
    }

    @SuppressLint("ShowToast")
    public static void showToast(Context ctx, String text, int duration) {
        if (sToast == null) {
            sToast = Toast.makeText(ctx.getApplicationContext(), R.string.app_name, duration);
        }
        sToast.setText(text);
        sToast.show();
    }
}

package com.hamzeh.easydriod.view;

import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import org.hamzeh.easydroid.R;

public class ViewVisibilityHandler implements Animation.AnimationListener {
    public static final String TAG = ViewVisibilityHandler.class.getSimpleName();
    private static final long DELAY = 25;
    private static final int SHOWN = 0;
    private static final int SHOWING = 1;
    private static final int HIDING = 2;
    private static final int HIDDEN = 3;

    private View mView;
    private String mTag;
    private Handler animationStartHandler;
    private int status;

    public ViewVisibilityHandler(View view) {
        mView = view;
        animationStartHandler = new Handler();
        status = view.getVisibility() == View.VISIBLE ? SHOWN : HIDDEN;
    }

    public ViewVisibilityHandler(View view, String tag) {
        this(view);
        mTag = tag;
    }

    public View getView() {
        return mView;
    }

    public void show() {
        show(R.anim.view_enter);
    }

    public void show(int animResId) {
        Log.i(TAG, mTag + " show()");
        if (status == SHOWN || status == SHOWING) {
            Log.e(TAG, mTag + " show() canceled! current status = " + statusAsString());
            return;
        }
        if (animResId == 0) {
            Log.i(TAG, mTag + " show(), Visibility -> View.VISIBLE, animResId == 0");
            mView.setVisibility(View.VISIBLE);
            status = SHOWN;
            return;
        }
        status = SHOWING;
        Animation anim = AnimationUtils.loadAnimation(mView.getContext(), animResId);
        anim.setAnimationListener(this);
        anim.setFillAfter(true);
        mView.clearAnimation();
        mView.startAnimation(anim);
    }

    public void hide(int animResId) {
        Log.i(TAG, mTag + " hide()");
        animationStartHandler.removeCallbacks(animationStartRunnable);
        if (status == HIDDEN || status == HIDING) {
            Log.e(TAG, mTag + " hide() canceled! current status = " + statusAsString());
            return;
        }
        if (animResId == 0) {
            Log.i(TAG, mTag + " hide(), Visibility -> View.GONE, animResId == 0");
            mView.setVisibility(View.GONE);
            status = HIDDEN;
            return;
        }
        status = HIDING;
        Animation anim = AnimationUtils.loadAnimation(mView.getContext(), animResId);
        anim.setAnimationListener(this);
        anim.setFillAfter(true);
        mView.clearAnimation();
        mView.startAnimation(anim);
    }

    @Override
    public void onAnimationStart(Animation animation) {
        Log.i(TAG, mTag + " onAnimationStart(), status = " + statusAsString());
        animationStartHandler.postDelayed(animationStartRunnable, DELAY);
    }

    private Runnable animationStartRunnable = new Runnable() {
        @Override
        public void run() {
            mView.setVisibility(View.VISIBLE);
            Log.i(TAG, mTag + " animationStartRunnable, Visibility -> View.VISIBLE, status = " + statusAsString());
        }
    };

    @Override
    public void onAnimationEnd(Animation animation) {
        mView.clearAnimation();
        if (status == SHOWING) {
            status = SHOWN;
            mView.setVisibility(View.VISIBLE);
            Log.i(TAG, mTag + " onAnimationEnd() status = " + statusAsString());
            Log.i(TAG, mTag + " Visibility -> View.VISIBLE, animation ended before animation start delay finished!");
        } else if (status == HIDING) {
            status = HIDDEN;
            mView.setVisibility(View.GONE);
            Log.i(TAG, mTag + " onAnimationEnd() status = " + statusAsString());
        }
        animationStartHandler.removeCallbacks(animationStartRunnable);
    }

    @Override
    public void onAnimationRepeat(Animation animation) {
    }

    private String statusAsString() {
        if (status == SHOWN) return "SHOWN";
        if (status == SHOWING) return "SHOWING";
        if (status == HIDING) return "HIDING";
        return "HIDDEN";
    }
}
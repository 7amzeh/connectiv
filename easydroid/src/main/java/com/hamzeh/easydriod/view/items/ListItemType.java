package com.hamzeh.easydriod.view.items;

public class ListItemType {
    public static int type;

    public Class<?> viewHolderClass;
    public int layoutResId;
    public int itemViewType;

    public ListItemType(Class<?> viewHolderClass, int layoutResId) {
        this.viewHolderClass = viewHolderClass;
        this.layoutResId = layoutResId;
        this.itemViewType = type++;
        ItemTypesPool.put(itemViewType, this);
    }

    @Override
    public int hashCode() {
        return itemViewType;
    }

    @Override
    public boolean equals(Object other) {
        return other == this || other instanceof ListItemType && itemViewType == ((ListItemType) other).itemViewType;
    }
}
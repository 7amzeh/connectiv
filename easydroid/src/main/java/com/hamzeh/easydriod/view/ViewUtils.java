package com.hamzeh.easydriod.view;

import android.animation.ArgbEvaluator;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.hamzeh.easydroid.R;

public class ViewUtils {
    public static void hideKeyboard(EditText... editTexts) {
        for (EditText editText : editTexts) {
            if (editText == null) {
                continue;
            }
            editText.clearFocus();
            InputMethodManager imm = (InputMethodManager) editText.getContext().getSystemService(Activity.INPUT_METHOD_SERVICE);
            if (imm != null) {
                imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);
            }
        }
    }

    public static void hideKeyboard(Context activityContext) {
        hideKeyboard(activityContext, null);
    }

    public static void hideKeyboard(Context activityContext, View parent) {
        if (activityContext instanceof Activity) {
            View view = ((Activity) activityContext).getCurrentFocus();
            if (view != null) {
                InputMethodManager imm = (InputMethodManager) activityContext.getSystemService(Context.INPUT_METHOD_SERVICE);
                if (imm != null) {
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }
                if (parent != null) {
                    parent.requestFocus();
                } else {
                    view.clearFocus();
                }
            }
        }
    }

    public static int getNavigationBarHeight(Context ctx) {
        Resources res = ctx.getResources();
        boolean navBarExists = res.getBoolean(res.getIdentifier("config_showNavigationBar", "bool", "android"));
        if (navBarExists) {
            int orientation = res.getConfiguration().orientation;
            int resourceId;
            if (isTablet(ctx)) {
                resourceId = res.getIdentifier(orientation == Configuration.ORIENTATION_PORTRAIT
                                ? "navigation_bar_height"
                                : "navigation_bar_height_landscape",
                        "dimen", "android");
            } else {
                resourceId = res.getIdentifier(orientation == Configuration.ORIENTATION_PORTRAIT
                                ? "navigation_bar_height"
                                : "navigation_bar_width",
                        "dimen", "android");
            }

            if (resourceId > 0) {
                return res.getDimensionPixelSize(resourceId);
            }
        }
        return 0;
    }

    public static boolean isTablet(Context c) {
        return (c.getResources().getConfiguration().screenLayout
                & Configuration.SCREENLAYOUT_SIZE_MASK) >= Configuration.SCREENLAYOUT_SIZE_LARGE;
    }

    public static void showKeyboard(EditText editText) {
        if (editText == null) {
            return;
        }
        editText.requestFocus();
        InputMethodManager keyboard = (InputMethodManager) editText.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        if (keyboard != null) {
            keyboard.showSoftInput(editText, 0);
        }
    }

    public static void setConditioned(TextView textView, String text, boolean isValid) {
        if (isValid) {
            textView.setVisibility(View.VISIBLE);
            textView.setText(text);
        } else {
            textView.setVisibility(View.GONE);
        }
    }

    public static void setWithDefault(TextView textView, String text, String defaultText) {
        boolean isValid = text != null && !text.trim().isEmpty();
        textView.setText(isValid ? text : defaultText);
    }

    public static void animateBackgroundColor(final View view, int colorFrom, int colorTo) {
        ValueAnimator colorAnimation = ValueAnimator.ofObject(new ArgbEvaluator(), colorFrom, colorTo);
        colorAnimation.setDuration(500);
        colorAnimation.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animator) {
                view.setBackgroundColor((int) animator.getAnimatedValue());
            }
        });
        colorAnimation.start();
    }

    public static Bitmap createDrawableFromView(Context context, View view) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay()
                .getMetrics(displayMetrics);
        view.measure(displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.layout(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
        // TODO: view.buildDrawingCache();
        Bitmap bitmap = Bitmap.createBitmap(view.getMeasuredWidth(),
                view.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        view.draw(canvas);
        return bitmap;
    }

    public static void pulseIcon(View v) {
        Animation pulse = AnimationUtils.loadAnimation(v.getContext(), R.anim.pulse_icon);
        v.clearAnimation();
        v.startAnimation(pulse);
    }

    public static void pulse(View v) {
        Animation pulse = AnimationUtils.loadAnimation(v.getContext(), R.anim.pulse);
        v.clearAnimation();
        v.startAnimation(pulse);
    }

    public static void pulseMini(View v) {
        Animation pulse = AnimationUtils.loadAnimation(v.getContext(), R.anim.pulse_mini);
        v.clearAnimation();
        v.startAnimation(pulse);
    }

    public static void pulseTiny(View v) {
        Animation pulse = AnimationUtils.loadAnimation(v.getContext(), R.anim.pulse_tiny);
        v.clearAnimation();
        v.startAnimation(pulse);
    }

    public static void shake(View v) {
        v.clearAnimation();
        v.startAnimation(AnimationUtils.loadAnimation(v.getContext(), R.anim.shake));
    }

    public static void lockEditTexts(EditText... editTexts) {
        for (EditText editText : editTexts) {
            lockEditText(editText);
        }
    }

    public static void lockEditText(EditText editText) {
        editText.setFocusable(false);
        editText.setFocusableInTouchMode(false);
        editText.setLongClickable(false);
    }

    public static void unlockEditTexts(EditText... editTexts) {
        for (EditText editText : editTexts) {
            unlockEditText(editText);
        }
    }

    public static void unlockEditText(EditText editText) {
        editText.setFocusable(true);
        editText.setFocusableInTouchMode(true);
        editText.setLongClickable(true);
    }

    public static void share(Context ctx, String subject, String body) {
        try {
            Intent i = new Intent(Intent.ACTION_SEND);
            i.setType("text/plain");
            i.putExtra(Intent.EXTRA_SUBJECT, subject);
            i.putExtra(Intent.EXTRA_TEXT, body);
            ctx.startActivity(Intent.createChooser(i, "Choose one..."));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Toast
     */
    private static Toast toast;
    private static String contextClassName;

    public static void showToast(Context context, int textResId) {
        showToast(context, context.getString(textResId));
    }

    @SuppressLint("ShowToast")
    public static void showToast(Context context, String text) {
        String contextClassName = (context instanceof Activity ? ((Activity) context).getClass().getName() : null);
        if (contextClassName == null) {
            return;
        }
        if (toast == null || !contextClassName.equals(ViewUtils.contextClassName)) {
            toast = Toast.makeText(context, R.string.app_name, Toast.LENGTH_SHORT);
            ViewUtils.contextClassName = contextClassName;
        }
        toast.setText(text);
        toast.show();
    }

    /**
     * Tool-Bar
     */
    public static int toolbarHeight = -1;

    public static int getToolbarHeight(Activity activity) {
        if (activity == null) return -1;
        if (toolbarHeight == -1) {
            TypedValue tv = new TypedValue();
            if (activity.getTheme().resolveAttribute(android.R.attr.actionBarSize, tv, true)) {
                toolbarHeight = TypedValue.complexToDimensionPixelSize(tv.data, activity.getResources().getDisplayMetrics());
            }
        }
        return toolbarHeight;
    }
}

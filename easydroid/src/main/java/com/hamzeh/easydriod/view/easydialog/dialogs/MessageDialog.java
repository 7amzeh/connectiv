package com.hamzeh.easydriod.view.easydialog.dialogs;

import android.content.Context;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hamzeh.easydriod.view.easydialog.utils.OptAnimationLoader;
import com.hamzeh.easydriod.view.easydialog.utils.SuccessTickView;
import com.pnikosis.materialishprogress.ProgressWheel;

import org.hamzeh.easydroid.R;

public class MessageDialog extends BaseDialog implements View.OnClickListener {
    public static final int STATE_LOADING = 1;
    public static final int STATE_ASKING = 2;
    public static final int STATE_MESSAGE = 3;
    public static final int STATE_RESULT_SUCCESS = 4;
    public static final int STATE_RESULT_FAILURE = 5;

    private TextView positiveButton;
    private TextView negativeButton;
    private TextView messageTextView;
    private View buttonsSeparatorView;

    private ProgressWheel progressWheel;
    private LinearLayout buttonsLayout;

    private SuccessTickView mSuccessTick;
    private View mSuccessLeftMask;
    private View mSuccessRightMask;
    private View mSuccessFrame;
    private AnimationSet mSuccessLayoutAnimSet;
    private Animation mSuccessBowAnim;

    private FrameLayout mErrorFrame;
    private ImageView mErrorX;
    private Animation mErrorInAnim;
    private AnimationSet mErrorXInAnim;

    private int mState;

    public MessageDialog(Context context, View.OnClickListener onClickListener) {
        super(context, onClickListener);
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.dialog_message;
    }

    @Override
    protected void setupView() {
        super.setupView();
        preparePositiveButton();
        prepareNegativeButton();
        messageTextView = dialogView.findViewById(R.id.txt_msg);
        progressWheel = dialogView.findViewById(R.id.progress_wheel);
        buttonsLayout = dialogView.findViewById(R.id.layout_buttons);
        buttonsSeparatorView = dialogView.findViewById(R.id.view_sep2);
        prepareErrorCross();
        prepareSuccessTick();
    }

    private void prepareErrorCross() {
        mErrorFrame = dialogView.findViewById(R.id.error_frame);
        mErrorX = dialogView.findViewById(R.id.error_x);
        mErrorInAnim = OptAnimationLoader.loadAnimation(getContext(), R.anim.dialog_error_frame_in);
        mErrorXInAnim = (AnimationSet) OptAnimationLoader.loadAnimation(getContext(), R.anim.dialog_error_x_in);
    }

    private void prepareSuccessTick() {
        mSuccessLayoutAnimSet = (AnimationSet) OptAnimationLoader.loadAnimation(getContext(),
                R.anim.dialog_success_mask_layout);
        mSuccessBowAnim = OptAnimationLoader.loadAnimation(getContext(), R.anim.dialog_success_bow_roate);
        mSuccessTick = dialogView.findViewById(R.id.success_tick);
        mSuccessFrame = dialogView.findViewById(R.id.success_frame);
        mSuccessLeftMask = mSuccessFrame.findViewById(R.id.mask_left);
        mSuccessRightMask = mSuccessFrame.findViewById(R.id.mask_right);
    }

    private void prepareNegativeButton() {
        negativeButton = dialogView.findViewById(R.id.btn_negative);
        negativeButton.setOnClickListener(this);
        negativeButton.setTag(mTag);
    }

    private void preparePositiveButton() {
        positiveButton = dialogView.findViewById(R.id.btn_positive);
        positiveButton.setOnClickListener(this);
        positiveButton.setTag(mTag);
    }

    @Override
    public void onClick(View view) {
        if (mOnClickListener != null
                && animationStatus == BaseDialog.STATUS_EXPANDED
                && mState != STATE_LOADING) {
            mOnClickListener.onClick(view);
        }
    }

    public MessageDialog setState(int state, int textResId) {
        setState(state, getContext().getString(textResId));
        return this;
    }

    public MessageDialog setState(int state, String extra) {
        mState = state;
        reset();
        switch (state) {
            case STATE_LOADING: {
                positiveButton.setVisibility(View.INVISIBLE);
                negativeButton.setVisibility(View.INVISIBLE);
                buttonsSeparatorView.setVisibility(View.INVISIBLE);
                messageTextView.setText(extra);
                progressWheel.setVisibility(View.VISIBLE);
                progressWheel.spin();
                break;
            }
            case STATE_ASKING: {
                messageTextView.setText(extra);
                break;
            }
            case STATE_MESSAGE: {
                buttonsSeparatorView.setVisibility(View.GONE);
                negativeButton.setVisibility(View.GONE);
                messageTextView.setText(extra);
                break;
            }
            case STATE_RESULT_SUCCESS: {
                buttonsSeparatorView.setVisibility(View.GONE);
                negativeButton.setVisibility(View.GONE);
                messageTextView.setText(extra);

                mSuccessFrame.setVisibility(View.VISIBLE);
                mSuccessLeftMask.startAnimation(mSuccessLayoutAnimSet.getAnimations().get(0));
                mSuccessRightMask.startAnimation(mSuccessLayoutAnimSet.getAnimations().get(1));
                mSuccessTick.startTickAnim();
                mSuccessRightMask.startAnimation(mSuccessBowAnim);
                break;
            }
            case STATE_RESULT_FAILURE: {
                buttonsSeparatorView.setVisibility(View.GONE);
                negativeButton.setVisibility(View.GONE);
                messageTextView.setText(extra);

                mErrorFrame.setVisibility(View.VISIBLE);
                mErrorFrame.startAnimation(mErrorInAnim);
                mErrorX.startAnimation(mErrorXInAnim);
                break;
            }
        }
        return this;
    }

    private void reset() {
        buttonsLayout.setVisibility(View.VISIBLE);
        buttonsSeparatorView.setVisibility(View.VISIBLE);
        positiveButton.setVisibility(View.VISIBLE);
        negativeButton.setVisibility(View.VISIBLE);

        progressWheel.stopSpinning();
        progressWheel.setVisibility(View.GONE);

        mErrorX.clearAnimation();
        mErrorFrame.clearAnimation();
        mErrorFrame.setVisibility(View.GONE);

        mSuccessTick.clearAnimation();
        mSuccessLeftMask.clearAnimation();
        mSuccessRightMask.clearAnimation();
        mSuccessFrame.setVisibility(View.GONE);
    }

    public int getState() {
        return mState;
    }

    public boolean isSuccess() {
        return mState == STATE_RESULT_SUCCESS;
    }

    public MessageDialog setPositive(int positiveResId) {
        positiveButton.setText(positiveResId);
        return this;
    }

    public MessageDialog setNegative(int negativeResId) {
        negativeButton.setText(negativeResId);
        return this;
    }
}
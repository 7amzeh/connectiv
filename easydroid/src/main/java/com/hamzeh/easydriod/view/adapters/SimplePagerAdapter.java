package com.hamzeh.easydriod.view.adapters;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.hamzeh.easydriod.controller.fragments.EasydroidFragment;

import java.util.ArrayList;
import java.util.List;

@Deprecated
public class SimplePagerAdapter extends FragmentPagerAdapter {
    private List<Fragment> mFragments = new ArrayList<>();
    private List<String> mFragmentTitles = new ArrayList<>();

    public SimplePagerAdapter(FragmentManager fm) {
        super(fm);
    }

    public void addFragment(Fragment fragment) {
        addFragment(fragment, "");
    }

    public void addFragment(Fragment fragment, String title) {
        mFragments.add(fragment);
        mFragmentTitles.add(title);
    }

    public SimplePagerAdapter clone(FragmentManager fm) {
        SimplePagerAdapter simplePagerAdapter = new SimplePagerAdapter(fm);
        simplePagerAdapter.setFragments(mFragments);
        simplePagerAdapter.setFragmentTitles(mFragmentTitles);
        return simplePagerAdapter;
    }

    @Override
    public Fragment getItem(int position) {
        return mFragments.get(position);
    }

    @Override
    public int getCount() {
        return mFragments.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mFragmentTitles.get(position);
    }

    public List<Fragment> getFragments() {
        return mFragments;
    }

    public void setFragments(List<Fragment> fragments) {
        mFragments = fragments;
    }

    public List<String> getFragmentTitles() {
        return mFragmentTitles;
    }

    public void setFragmentTitles(List<String> fragmentTitles) {
        mFragmentTitles = fragmentTitles;
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        for (Fragment fragment : mFragments) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        for (Fragment fragment : mFragments) {
            fragment.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    public void onPageSelected(int position) {
        Fragment fragment = mFragments.get(position);
        if (fragment instanceof EasydroidFragment) {
            ((EasydroidFragment) fragment).onPageSelected();
        }
    }
}
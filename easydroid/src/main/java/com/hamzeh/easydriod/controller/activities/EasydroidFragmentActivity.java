package com.hamzeh.easydriod.controller.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;

import com.hamzeh.easydriod.controller.location.EasydroidLocations;
import com.hamzeh.easydriod.controller.utils.EasydroidMediaPicker;
import com.hamzeh.easydriod.controller.utils.EasydroidPermissions;

import org.hamzeh.easydroid.R;

public class EasydroidFragmentActivity extends EasydroidActivity {
    public static final String FRAGMENT_NAME = "FRAGMENT_NAME";

    protected Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutResId());
        prepareToolbar();

        if (savedInstanceState == null) {
            String fragmentName = getIntent().getStringExtra(FRAGMENT_NAME);
            if (fragmentName != null) {
                FragmentManager fragmentManager = getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                Fragment fragment = Fragment.instantiate(this, fragmentName);
                fragment.setArguments(getIntent().getExtras());
                fragmentTransaction.replace(R.id.container, fragment);
                fragmentTransaction.commit();
            }
        }
    }

    private void prepareToolbar() {
        toolbar = findViewById(R.id.toolbar);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
        }
    }

    protected int getLayoutResId() {
        return R.layout.activity_easydroid_fragment;
    }
}

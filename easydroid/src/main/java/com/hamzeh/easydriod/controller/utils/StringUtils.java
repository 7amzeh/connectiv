package com.hamzeh.easydriod.controller.utils;

import android.content.Context;
import android.text.TextUtils;
import android.util.Base64;

import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringUtils {
    public static int countWord(String word, String text) {
        int i = 0;
        Pattern p = Pattern.compile(word);
        Matcher m = p.matcher(text);
        while (m.find()) {
            i++;
        }
        return i;
    }

    public static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    public static String pojoToJson(Object obj) {
        return new Gson().toJson(obj);
    }

    public static String fromInputStream(InputStream in) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(in));
        StringBuilder sbResponse = new StringBuilder();
        String line;
        while ((line = br.readLine()) != null) {
            sbResponse.append(line).append("\r\n");
        }
        br.close();
        return sbResponse.toString().trim();
    }

    public static String buildRepeatedParam(String paramKey, List<Long> params) {
        List<String> paramsStringList = new ArrayList<>();
        final String separator = "_AND_" + paramKey + "_EQU_";
        for (long param : params) {
            paramsStringList.add(String.valueOf(param));
        }
        return TextUtils.join(separator, paramsStringList);
    }

    public static String trimStartingZeros(String str) {
        return str.replaceFirst("^0+(?!$)", "");
    }

    public static String urlDecode(String text) {
        try {
            return URLDecoder.decode(text, "UTF-8");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String urlEncode(String text) {
        try {
            return URLEncoder.encode(text, "UTF-8");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String fromAssets(Context context, String fileName) {
        try {
            StringBuilder sb = new StringBuilder();
            InputStream json = context.getAssets().open("json/" + fileName);
            BufferedReader in = new BufferedReader(new InputStreamReader(json, "UTF-8"));
            String line;
            while ((line = in.readLine()) != null) {
                sb.append(line);
            }
            in.close();
            return sb.toString().trim();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getFileName(String filePath) {
        return filePath.substring(filePath.lastIndexOf("/") + 1);
    }

    public static String formatTime(int millis) {
        int seconds = millis / 1000;
        int minutes = seconds / 60;
        int hours = minutes / 60;

        String formattedMinsSecs = String.format(Locale.ENGLISH, "%02d:%02d", minutes % 60, seconds % 60);
        String formattedHours = String.format(Locale.ENGLISH, "%02d:", hours);
        return (formattedHours.startsWith("00") ? "" : formattedHours) + formattedMinsSecs;
    }

    public static String toBase64(String text) {
        try {
            byte[] data = text.getBytes("UTF-8");
            return Base64.encodeToString(data, Base64.DEFAULT);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String fromBase64(String text) {
        try {
            byte[] data = Base64.decode(text, Base64.DEFAULT);
            return new String(data);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}

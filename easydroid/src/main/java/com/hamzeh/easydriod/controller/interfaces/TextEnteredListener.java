package com.hamzeh.easydriod.controller.interfaces;

public interface TextEnteredListener {
    void onTextEntered(String text);

    void onTextChanged(String text);
}
package com.hamzeh.easydriod.controller.utils;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;

import org.hamzeh.easydroid.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EasydroidPermissions {
    public static final int REQUEST_CODE_PERMISSIONS = 20202;

    private static EasydroidPermissions mPermissionHandler;

    private GrantPermissionCallback mCallback;
    private String[] mPermissionsToGrant;
    private int mRequestCode;

    public static EasydroidPermissions getInstance() {
        if (mPermissionHandler == null) {
            mPermissionHandler = new EasydroidPermissions();
        }
        return mPermissionHandler;
    }

    public void checkPermissions(Activity activity,
                                 GrantPermissionCallback callback,
                                 String... permissions) {
        checkPermissions(activity, callback, REQUEST_CODE_PERMISSIONS, permissions);
    }

    public void checkPermissions(Activity activity, GrantPermissionCallback callback,
                                 int requestCode, String... permissions) {
        mRequestCode = requestCode;
        mCallback = callback;
        mPermissionsToGrant = permissions;
        List<String> reasonPermissions = new ArrayList<>();
        int nonGrantedCount = 0;

        for (String permission : mPermissionsToGrant) {
            boolean granted = ActivityCompat.checkSelfPermission(activity, permission)
                    == PackageManager.PERMISSION_GRANTED;
            if (!granted) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(activity, permission)) {
                    int readablePermissionResId = sReadablePermissionMap.get(permission);
                    reasonPermissions.add(activity.getString(readablePermissionResId));
                }
                nonGrantedCount++;
            }
        }

        if (!reasonPermissions.isEmpty()) {
            showReasonDialog(activity, reasonPermissions);
            return;
        }

        if (nonGrantedCount > 0) {
            ActivityCompat.requestPermissions(activity, mPermissionsToGrant, mRequestCode);
            return;
        }
        mCallback.onGrantPermissionsResult(mRequestCode, true, Arrays.asList(permissions));
    }

    private void showReasonDialog(final Activity activity, List<String> reasonPermissions) {
        String message = activity.getString(R.string.permission_dialog_msg);
        String joinedPermissions = TextUtils.join("\n• ", reasonPermissions);
        message = message.concat("\n\n• ").concat(joinedPermissions);

        new AlertDialog.Builder(activity)
                .setMessage(message)
                .setPositiveButton(activity.getString(R.string.got_it), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ActivityCompat.requestPermissions(activity, mPermissionsToGrant, mRequestCode);
                    }
                })
                .create()
                .show();
    }

    private static Map<String, Integer> sReadablePermissionMap = new HashMap<String, Integer>() {
        {
            put(Manifest.permission.CAMERA, R.string.permission_camera);
            put(Manifest.permission.USE_SIP, R.string.permission_use_sip);
            put(Manifest.permission.SEND_SMS, R.string.permission_send_sms);
            put(Manifest.permission.CALL_PHONE, R.string.permission_call_phone);
            put(Manifest.permission.RECEIVE_SMS, R.string.permission_receive_sms);
            put(Manifest.permission.RECORD_AUDIO, R.string.permission_microphone);
            put(Manifest.permission.RECEIVE_MMS, R.string.permission_receive_mms);
            put(Manifest.permission.GET_ACCOUNTS, R.string.permission_get_accounts);
            put(Manifest.permission.RECEIVE_WAP_PUSH, R.string.permission_wap_push);
            put(Manifest.permission.READ_CALENDAR, R.string.permission_read_calendar);
            put(Manifest.permission.READ_CONTACTS, R.string.permission_read_contacts);
            put(Manifest.permission.READ_CALL_LOG, R.string.permission_read_call_log);
            put(Manifest.permission.READ_PHONE_STATE, R.string.permission_phone_state);
            put(Manifest.permission.ADD_VOICEMAIL, R.string.permission_add_voice_mail);
            put(Manifest.permission.WRITE_CALENDAR, R.string.permission_write_calender);
            put(Manifest.permission.WRITE_CONTACTS, R.string.permission_write_contacts);
            put(Manifest.permission.WRITE_CALL_LOG, R.string.permission_write_call_log);
            put(Manifest.permission.READ_EXTERNAL_STORAGE, R.string.permission_read_storage);
            put(Manifest.permission.ACCESS_FINE_LOCATION, R.string.permission_fine_location);
            put(Manifest.permission.WRITE_EXTERNAL_STORAGE, R.string.permission_write_storage);
            put(Manifest.permission.PROCESS_OUTGOING_CALLS, R.string.permission_outgoing_calls);
            put(Manifest.permission.ACCESS_COARSE_LOCATION, R.string.permission_coarse_location);
        }
    };

    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (mRequestCode == requestCode && mCallback != null) {
            List<String> grantedPermissions = new ArrayList<>();
            for (int i = 0; i < grantResults.length; i++) {
                int result = grantResults[i];
                if (result == PackageManager.PERMISSION_GRANTED) {
                    grantedPermissions.add(permissions[i]);
                }
            }
            boolean grantedAll = grantedPermissions.size() == permissions.length;
            mCallback.onGrantPermissionsResult(requestCode, grantedAll, grantedPermissions);
        }
    }

    public interface GrantPermissionCallback {
        void onGrantPermissionsResult(int requestCode, boolean grantedAll, List<String> grantedPermissions);
    }
}
package com.hamzeh.easydriod.controller.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.util.ObjectsCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import com.hamzeh.easydriod.controller.enums.ResponseMode;
import com.hamzeh.easydriod.controller.interfaces.ListItemCallback;
import com.hamzeh.easydriod.controller.interfaces.LoadMore;
import com.hamzeh.easydriod.controller.utils.AppUtils;
import com.hamzeh.easydriod.model.server.Response;
import com.hamzeh.easydriod.model.server.WebService;
import com.hamzeh.easydriod.view.LoadingUtils;
import com.hamzeh.easydriod.view.adapters.EasydroidAdapter;
import com.hamzeh.easydriod.view.items.ListItem;
import com.hamzeh.easydriod.view.items.LoadingMore;

import org.hamzeh.easydroid.R;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

public abstract class EasydroidListFragment extends EasydroidFragment implements
        SwipeRefreshLayout.OnRefreshListener,
        ListItemCallback,
        LoadMore {
    private static final int MIN_PAGE_SIZE = 10;

    protected RecyclerView mRecyclerView;
    protected EasydroidAdapter mEasydroidAdapter;
    protected SwipeRefreshLayout mSwipeRefreshLayout;
    protected SparseArray<List<ListItem>> mPagesData = new SparseArray<>();
    protected ListOptions mListOptions;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mListOptions = new ListOptions();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_list, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        prepareRecyclerView();
        initRefreshLayout();

        if (mEasydroidAdapter == null || mEasydroidAdapter.empty()) {
            setLoadingUtilsState(LoadingUtils.State.LOADING);
            if (!onPreInitialRequest()) {
                requestItems();
            }
        }

        mLoadingUtils.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                mRecyclerView.dispatchTouchEvent(event);
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    int duration = (int) (event.getEventTime() - event.getDownTime());
                    Log.i("Duration", "D = " + duration);
                    if (duration < 110) {
                        mLoadingUtils.callOnClick();
                    }
                }
                return true;
            }
        });
    }

    protected boolean onPreInitialRequest() {
        return false;
    }

    protected void setLayoutManager() {
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
    }

    @Override
    public void onSuccess(Response response) {
        super.onSuccess(response);
        if (ObjectsCompat.equals(response.webService, getWebService())) {
            onListSuccess(response);
        }
    }

    protected void onListSuccess(Response response) {
        refreshingDone();
        mListOptions.setIsLoadingMore(false);
        setLoadingUtilsState(LoadingUtils.State.DONE);
        List<ListItem> items = getItems(response);

        if (response.responseMode == ResponseMode.LIVE && items.isEmpty()) {
            boolean isFirst = (mListOptions.mPageNum == mListOptions.mFromPage);
            boolean noPreviousData = mEasydroidAdapter.empty();
            if (isFirst && noPreviousData) {
                setLoadingUtilsState(LoadingUtils.State.NO_DATA);
                return;
            }
        }

        if (mListOptions.isRefreshing()) {
            mPagesData.clear();
            mListOptions.toPageNum(Integer.MAX_VALUE);
            mListOptions.setIsRefreshing(false);
        }

        mPagesData.put(mListOptions.mPageNum, items);
        if (!hasNext(response) || items.size() < MIN_PAGE_SIZE) {
            mListOptions.stopPagination();
        }
        boolean loadMore = ObjectsCompat.equals(response.webService, getWebService());
        drawItems(combinePages(), loadMore);
    }

    @Override
    public void onFailure(Response response) {
        super.onFailure(response);
        if (ObjectsCompat.equals(response.webService, getWebService())) {
            onListFailure(response);
        }
    }

    protected void onListFailure(Response response) {
        refreshingDone();
        mListOptions.setIsLoadingMore(false);

        if (response.shouldShowUiError()) {
            boolean isFirst = (mListOptions.mPageNum == mListOptions.mFromPage);
            boolean noPreviousData = mEasydroidAdapter.empty();
            boolean isInternetConnected = AppUtils.isNetworkConnected(getContext());
            if (isFirst && noPreviousData) {
                setLoadingUtilsState(isInternetConnected
                        ? LoadingUtils.State.ERROR
                        : LoadingUtils.State.NO_CONNECTION);
            }
        }
    }

    protected List<ListItem> combinePages() {
        Set<ListItem> combined = new LinkedHashSet<>();
        for (int i = 0; i < mPagesData.size(); i++) {
            combined.addAll(mPagesData.valueAt(i));
        }
        return new ArrayList<>(combined);
    }

    protected void drawItems(List<ListItem> items, boolean loadMore) {
        if (!isAdded()) {
            Log.e(TAG, "trying to draw while the fragment isn't added! this can cause a crash.");
            return;
        }
        if (loadMore && mListOptions.mPageNum < mListOptions.mToPage && items.size() >= MIN_PAGE_SIZE) {
            items.add(new LoadingMore(this));
            mListOptions.mPageNum++;
        }
        mEasydroidAdapter.replaceContents(items, true);
    }

    protected void prepareRecyclerView() {
        mRecyclerView = mRoot.findViewById(R.id.rv);
        if (mRecyclerView != null) {
            mEasydroidAdapter = new EasydroidAdapter(getContext(), new ArrayList<ListItem>(), this);
            mEasydroidAdapter.setHasStableIds(true);
            mRecyclerView.setHasFixedSize(true);
            mRecyclerView.setAdapter(mEasydroidAdapter);
            addItemDecoration();
            setLayoutManager();
        }
    }

    @Override
    public void onItemClicked(View view, ListItem listItem, int position) {
    }

    @Override
    public void onItemAction(int action, View view, Bundle data) {
    }

    @Override
    public void loadMore() {
        if (!mListOptions.isIsLoadingMore()) {
            mListOptions.setIsLoadingMore(true);
            requestItems();
        }
    }

    @Override
    public void onReload() {
        super.onReload();
        requestItems();
    }

    protected void clearList() {
        mEasydroidAdapter.replaceContents(new ArrayList<ListItem>(), true);
    }

    /*** Refreshing */

    protected void initRefreshLayout() {
        mSwipeRefreshLayout = mRoot.findViewById(R.id.refresh);
        if (mSwipeRefreshLayout != null) {
            mSwipeRefreshLayout.setOnRefreshListener(this);
            mSwipeRefreshLayout.setEnabled(mListOptions.mPullToRefreshEnabled);
        }
    }

    protected void refreshingDone() {
        if (mSwipeRefreshLayout != null) {
            mSwipeRefreshLayout.setRefreshing(false);
        }
    }

    @Override
    public void onRefresh() {
        mListOptions.onRefresh();
    }

    /*** End-Refreshing */

    @Override
    public void onDestroyView() {
        mListOptions.release();
        super.onDestroyView();
    }

    public class ListOptions {
        private int mPageNum;
        private int mFromPage;
        private int mToPage = Integer.MAX_VALUE;
        private boolean mPullToRefreshEnabled;

        private boolean mIsLoadingMore;
        private boolean mIsRefreshing;

        public ListOptions enablePullToRefresh(boolean enabled) {
            mPullToRefreshEnabled = enabled;
            if (mSwipeRefreshLayout != null) {
                mSwipeRefreshLayout.setEnabled(enabled);
            }
            return this;
        }

        public ListOptions fromPageNum(int fromPage) {
            mFromPage = fromPage;
            mPageNum = fromPage;
            return this;
        }

        public void setPageNum(int pageNum) {
            mPageNum = pageNum;
        }

        public ListOptions toPageNum(int toPage) {
            mToPage = toPage;
            return this;
        }

        public int getPageNum() {
            return mPageNum;
        }

        public void stopPagination() {
            toPageNum(mPageNum);
        }

        /*** Getters & Setters */

        public boolean isIsLoadingMore() {
            return mIsLoadingMore;
        }

        public void setIsLoadingMore(boolean isLoadingMore) {
            mIsLoadingMore = isLoadingMore;
        }

        public boolean isRefreshing() {
            return mIsRefreshing;
        }

        public void setIsRefreshing(boolean isRefreshing) {
            mIsRefreshing = isRefreshing;
        }

        public void release() {
            mIsLoadingMore = false;
            mIsRefreshing = false;
        }

        public void onRefresh() {
            mPageNum = mFromPage;
            mIsLoadingMore = false;
            mIsRefreshing = true;
        }
    }

    abstract protected WebService getWebService();

    abstract protected void requestItems();

    abstract protected List<ListItem> getItems(Response response);

    abstract protected boolean hasNext(Response response);

    abstract protected void addItemDecoration();
}
package com.hamzeh.easydriod.controller.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.text.Spannable;
import android.text.SpannableString;

import com.hamzeh.easydriod.controller.enums.RequestMode;
import com.hamzeh.easydriod.controller.location.EasydroidLocations;
import com.hamzeh.easydriod.controller.utils.EasydroidMediaPicker;
import com.hamzeh.easydriod.controller.utils.EasydroidPermissions;
import com.hamzeh.easydriod.model.server.RequestManager;
import com.hamzeh.easydriod.model.server.Response;
import com.hamzeh.easydriod.controller.interfaces.AdaptableTitle;
import com.hamzeh.easydriod.controller.interfaces.OnResponseReady;
import com.hamzeh.easydriod.model.server.WebService;
import com.hamzeh.easydriod.view.FontUtils;
import com.hamzeh.easydriod.view.TypefaceSpan;

public abstract class EasydroidActivity extends AppCompatActivity implements
        OnResponseReady, AdaptableTitle {
    protected String TAG = EasydroidActivity.class.getSimpleName();

    protected Context mContext = this;
    protected RequestManager mRequestManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        super.onCreate(savedInstanceState);
        TAG = getClass().getSimpleName();
        mRequestManager = new RequestManager(getCacheDir().getPath());
    }

    @Override
    public void onSuccess(Response response) {
    }

    @Override
    public void onFailure(Response response) {
    }

    /*** Requests Management */

    protected void sendRequest(WebService webService, String... values) {
        sendRequest(webService, this, values);
    }

    protected void sendRequest(WebService webService, OnResponseReady onResponseReady, String... values) {
        sendRequest(webService, onResponseReady, RequestMode.LIVE_ONLY, values);
    }

    protected void sendRequest(WebService webService, OnResponseReady onResponseReady,
                               RequestMode requestMode, String... values) {
        mRequestManager.builder()
                .webService(webService)
                .callback(onResponseReady)
                .requestMode(requestMode)
                .values(values)
                .send();
    }

    @Override
    public void setActionbarTitle(int titleResId) {
        setActionbarTitle(getString(titleResId));
    }

    @Override
    public void setActionbarTitle(String title) {
        if (getSupportActionBar() != null) {
            SpannableString spannableString = new SpannableString(title);
            spannableString.setSpan(new TypefaceSpan(this,
                            FontUtils.CustomFont.OPEN_SANS_REGULAR.getFontName()), 0,
                    spannableString.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            getSupportActionBar().setTitle(spannableString);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasydroidPermissions.getInstance().onRequestPermissionsResult(requestCode,
                permissions, grantResults);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        EasydroidMediaPicker.getInstance().onActivityResult(mContext, requestCode, resultCode, data);
        EasydroidLocations.getInstance().onActivityResult(mContext, requestCode, resultCode, data);
    }

    @Override
    protected void onDestroy() {
        mRequestManager.cancelTasks();
        super.onDestroy();
    }
}

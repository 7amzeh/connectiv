package com.hamzeh.easydriod.controller.location;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.util.Log;

import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnCanceledListener;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.hamzeh.easydriod.controller.utils.EasydroidPermissions;

import org.hamzeh.easydroid.R;

import java.util.List;

public class EasydroidLocations {
    private static final String TAG = EasydroidLocations.class.getSimpleName();

    private static final int REQUEST_CODE_LOCATION_SETTINGS = 10011;

    private static final long DEFAULT_UPDATE_INTERVAL = 3000;
    private static final long DEFAULT_FASTEST_INTERVAL = 3000;

    public static final int ERROR_GPS_PROBLEM = 1;
    public static final int ERROR_PERMISSION_DENIED = 2;
    public static final int ERROR_USER_DENIED_ENABLING_GPS = 3;
    public static final int ERROR_LOCATION_UPDATE_CANCELED = 4;
    public static final int ERROR_LOCATION_UPDATE_FAILED = 5;
    public static final int ERROR_LOCATION_NULL = 6;

    private LocationUpdatedCallback mLocationUpdatedCallback;
    private FusedLocationProviderClient mFusedLocationClient;
    private LocationRequest mLocationRequest;

    private long mUpdateInterval = -1;
    private long mFastestInterval = -1;

    private int mRequestCode = -1;

    private boolean mContinuous;

    private static EasydroidLocations sEasydroidLocations;

    public static EasydroidLocations getInstance() {
        if (sEasydroidLocations == null) {
            sEasydroidLocations = new EasydroidLocations();
        }
        return sEasydroidLocations;
    }

    public void startTracking(Context context, LocationUpdatedCallback callback) {
        mLocationUpdatedCallback = callback;

        if (mUpdateInterval == -1) mUpdateInterval = DEFAULT_UPDATE_INTERVAL;
        if (mFastestInterval == -1) mFastestInterval = DEFAULT_FASTEST_INTERVAL;
        if (mRequestCode == -1) mRequestCode = REQUEST_CODE_LOCATION_SETTINGS;

        initLocationRequest(context);
        checkLocationPermission(context);
    }

    private void initLocationRequest(Context context) {
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(context);
        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(mUpdateInterval)
                .setFastestInterval(mFastestInterval);
    }

    private void checkLocationPermission(final Context context) {
        Activity activity = ((Activity) context);
        if (activity == null || activity.isDestroyed() || activity.isFinishing()) {
            Log.e(TAG, "needsLocationPermission: activity is null or destroyed!");
            return;
        }
        EasydroidPermissions.getInstance().checkPermissions(activity, new EasydroidPermissions.GrantPermissionCallback() {
                    @Override
                    public void onGrantPermissionsResult(int requestCode, boolean grantedAll, List<String> grantedPermissions) {
                        if (grantedAll) {
                            checkLocationSettings(context);
                        } else {
                            mLocationUpdatedCallback.onTrackingFailed(ERROR_PERMISSION_DENIED);
                        }
                    }
                },
                Manifest.permission.ACCESS_FINE_LOCATION);
    }

    private void checkLocationSettings(final Context context) {
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);
        LocationSettingsRequest locationSettingsRequest = builder.build();
        SettingsClient settingsClient = LocationServices.getSettingsClient(context);
        settingsClient.checkLocationSettings(locationSettingsRequest)
                .addOnSuccessListener(new OnSuccessListener<LocationSettingsResponse>() {
                    @Override
                    public void onSuccess(LocationSettingsResponse response) {
                        Log.i(TAG, "checkLocationSettings: onSuccess");
                        LocationSettingsStates statuses = response.getLocationSettingsStates();
                        if (statuses.isGpsPresent() && statuses.isGpsUsable()) {
                            startLocationUpdates();
                        } else {
                            mLocationUpdatedCallback.onTrackingFailed(ERROR_GPS_PROBLEM);
                        }
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.i(TAG, "checkLocationSettings: onFailure");
                        onCheckLocationSettingsFailure(context, (ApiException) e);
                    }
                });
    }

    private void onCheckLocationSettingsFailure(Context context, ApiException e) {
        Activity activity = ((Activity) context);
        if (activity == null || activity.isDestroyed() || activity.isFinishing()) {
            return;
        }

        int statusCode = e.getStatusCode();
        switch (statusCode) {
            case CommonStatusCodes.RESOLUTION_REQUIRED:
                Log.e(TAG, "Location settings not satisfied, attempting resolution intent");
                try {
                    ResolvableApiException resolvable = (ResolvableApiException) e;
                    resolvable.startResolutionForResult(activity, REQUEST_CODE_LOCATION_SETTINGS);
                } catch (Exception ex) {
                    Log.e(TAG, "Unable to start resolution intent");
                    showEnableGpsDialog(context);
                }
                break;
            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                Log.e(TAG, "Location settings not satisfied and can't be changed");
                showEnableGpsDialog(context);
                break;
            default: {
                showEnableGpsDialog(context);
            }
        }
    }

    private void showEnableGpsDialog(final Context context) {
        String message = context.getString(R.string.msg_gps_off);
        new AlertDialog.Builder(context)
                .setMessage(message)
                .setPositiveButton(context.getString(R.string.answer_yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        context.startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton(context.getString(R.string.answer_no), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        mLocationUpdatedCallback.onTrackingFailed(ERROR_USER_DENIED_ENABLING_GPS);
                    }
                })
                .create()
                .show();
    }

    @SuppressLint("MissingPermission")
    private void startLocationUpdates() {
        mLocationUpdatedCallback.onTrackingStarted();
        mFusedLocationClient.requestLocationUpdates(mLocationRequest, mLocationCallback, Looper.myLooper())
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.i(TAG, "requestLocationUpdates: onSuccess");
                    }
                })
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        Log.i(TAG, "requestLocationUpdates: onComplete");
                    }
                })
                .addOnCanceledListener(new OnCanceledListener() {
                    @Override
                    public void onCanceled() {
                        Log.i(TAG, "requestLocationUpdates: onCanceled");
                        mLocationUpdatedCallback.onTrackingFailed(ERROR_LOCATION_UPDATE_CANCELED);
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.i(TAG, "requestLocationUpdates: onFailure");
                        mLocationUpdatedCallback.onTrackingFailed(ERROR_LOCATION_UPDATE_FAILED);
                    }
                });
    }

    private LocationCallback mLocationCallback = new LocationCallback() {
        @Override
        public void onLocationResult(LocationResult locationResult) {
            super.onLocationResult(locationResult);
            if (locationResult.getLastLocation() != null) {
                if (!mContinuous) {
                    mFusedLocationClient.removeLocationUpdates(mLocationCallback);
                }
                Location location = locationResult.getLastLocation();
                mLocationUpdatedCallback.onLocationUpdated(location);
            } else {
                mLocationUpdatedCallback.onTrackingFailed(ERROR_LOCATION_NULL);
            }
        }
    };

    public void stopTracking() {
        if (mFusedLocationClient != null) {
            mFusedLocationClient.removeLocationUpdates(mLocationCallback);
        }
    }

    public void onActivityResult(Context context, int requestCode, int resultCode, Intent data) {
        if (requestCode != mRequestCode) {
            return;
        }
        if (resultCode != Activity.RESULT_OK) {
            mLocationUpdatedCallback.onTrackingFailed(ERROR_USER_DENIED_ENABLING_GPS);
            return;
        }
        checkLocationPermission(context);
    }

    public interface LocationUpdatedCallback {
        void onTrackingStarted();

        void onLocationUpdated(Location location);

        void onTrackingFailed(int error);
    }

    /*** Builder */

    public EasydroidLocations updateInterval(long updateInterval) {
        mUpdateInterval = updateInterval;
        return this;
    }

    public EasydroidLocations fastestInterval(long fastestInterval) {
        mFastestInterval = fastestInterval;
        return this;
    }

    public EasydroidLocations requestCode(int requestCode) {
        mRequestCode = requestCode;
        return this;
    }

    public EasydroidLocations continuous(boolean continuous) {
        mContinuous = continuous;
        return this;
    }
}
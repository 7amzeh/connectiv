package com.hamzeh.easydriod.controller.fragments;

import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.hamzeh.easydriod.controller.enums.RequestMode;
import com.hamzeh.easydriod.model.server.Response;
import com.hamzeh.easydriod.controller.interfaces.AdaptableTitle;
import com.hamzeh.easydriod.controller.interfaces.OnResponseReady;
import com.hamzeh.easydriod.model.server.WebService;
import com.hamzeh.easydriod.controller.utils.EasydroidPermissions;
import com.hamzeh.easydriod.model.server.RequestManager;
import com.hamzeh.easydriod.view.LoadingUtils;
import com.hamzeh.easydriod.view.easydialog.dialogs.BaseDialog;
import com.hamzeh.easydriod.view.easydialog.dialogs.MessageDialog;

import org.hamzeh.easydroid.R;

public class EasydroidFragment extends Fragment implements
        AppBarLayout.OnOffsetChangedListener,
        LoadingUtils.OnReloadListener,
        BaseDialog.OnClickListener,
        OnResponseReady {
    private static final long DELAY_FRAGMENT_NAVIGATION = 250;
    private static final int TIME_TO_GO_BACK = 250;

    public String TAG = EasydroidFragment.class.getSimpleName();
    public static boolean isAnimationDisabled;

    protected View mRoot;
    protected LoadingUtils mLoadingUtils;
    protected RequestManager mRequestManager;
    protected Toolbar mToolbar;
    protected Handler mDelayHandler;

    protected boolean mHasBeenSelected;
    protected boolean mIsSelected;
    protected boolean mViewCreated;
    protected boolean mIsNavigating;

    private int mToolbarHeight;

    protected MessageDialog mMessageDialog;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mRequestManager = new RequestManager(context.getCacheDir().getPath());
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        TAG = getClass().getSimpleName();
        mDelayHandler = new Handler();
        initMessageDialog();
        calcToolbarHeight();
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mRoot = view;
        prepareToolbar();
        initLoadingUtils();
        mViewCreated = true;
    }

    @Override
    public void onResume() {
        super.onResume();
        setAppBarExpanded(true);
    }

    @Override
    public void onSuccess(Response response) {
    }

    @Override
    public void onFailure(Response response) {
    }

    private void initMessageDialog() {
        mMessageDialog = new MessageDialog(getContext(), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (v.getId() == R.id.btn_positive) {
                    onDialogPositive();
                } else if (v.getId() == R.id.btn_negative) {
                    onDialogNegative();
                }
            }
        });
    }

    protected void prepareToolbar() {
        mToolbar = mRoot.findViewById(R.id.toolbar);
        if (getActivity() instanceof AppCompatActivity && mToolbar != null) {
            AppCompatActivity activity = (AppCompatActivity) getActivity();
            activity.setSupportActionBar(mToolbar);
        }
    }

    protected void initLoadingUtils() {
        mLoadingUtils = mRoot.findViewById(R.id.loading_utils);
        if (mLoadingUtils != null) {
            mLoadingUtils.setOnReloadListener(this);
        }
    }

    protected void setLoadingUtilsTitle(int titleResId) {
        if (mLoadingUtils != null) {
            mLoadingUtils.setTitle(titleResId);
        }
    }

    /**
     * Loading-Dialog-Stuff
     */

    protected void goBackDelayed() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (getActivity() != null) {
                    getActivity().onBackPressed();
                }
            }
        }, TIME_TO_GO_BACK);
    }

    public void onPageSelected() {
    }

    @Override
    public void onReload() {
    }

    @SuppressWarnings("unchecked")
    protected <T extends View> T findViewById(@IdRes int id) {
        return (T) mRoot.findViewById(id);
    }

    /**
     * Actionbar Title
     */
    public void setActionbarTitle(int titleResId) {
        setActionbarTitle(getString(titleResId));
    }

    public void setActionbarTitle(String title) {
        if (getActivity() instanceof AdaptableTitle) {
            ((AdaptableTitle) getActivity()).setActionbarTitle(title);
        }
    }

    /**
     * App-Bar
     */

    public static final int TOOLBAR_DELAY = 50;
    public static boolean lockToolbar;
    public static boolean sDown;
    protected Handler appBarHandler = new Handler();
    protected AppBarLayout mAppBarLayout;
    protected int appBarOffset, appDirection;

    private Runnable appBarRunnable = new Runnable() {
        @Override
        public void run() {
            Log.i("DIR", "Direction: " + appDirection);
            if (sDown) {
                appBarHandler.postDelayed(this, TOOLBAR_DELAY);
            } else {
                setAppBarExpanded(Math.abs(appBarOffset) < mToolbarHeight / 2);
            }
        }
    };

    public void onAppbarOffsetChanged(int appBarOffset) {
        if (this.appBarOffset != appBarOffset) {
            appDirection = (int) Math.signum(appBarOffset - this.appBarOffset);
            this.appBarOffset = appBarOffset;
            if (mAppBarLayout != null) {
                appBarHandler.removeCallbacks(appBarRunnable);
                if (appBarOffset != 0 && appBarOffset != -mToolbarHeight) {
                    appBarHandler.postDelayed(appBarRunnable, TOOLBAR_DELAY);
                }
            }
        }
    }

    public int getAppBarOffset() {
        return appBarOffset;
    }

    public void setAppBarOffset(int appBarOffset) {
        this.appBarOffset = appBarOffset;
    }

    protected void initAppBar(AppBarLayout appBarLayout) {
        if (appBarLayout != null) {
            mAppBarLayout = appBarLayout;
            mAppBarLayout.removeOnOffsetChangedListener(this);
            mAppBarLayout.addOnOffsetChangedListener(this);
        }
    }

    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
        if (appBarLayout != null) {
            if (lockToolbar && this.appBarOffset == 0 && this.appBarOffset != verticalOffset) {
                mAppBarLayout.setExpanded(true, false);
            } else {
                onAppbarOffsetChanged(verticalOffset);
            }
        }
    }

    protected void setAppBarExpanded(boolean isExpanded) {
        if (mAppBarLayout != null) {
            mAppBarLayout.setExpanded(isExpanded, true);
        }
    }

    /*** Handles transition animation */

    @Override
    public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
        if (isAnimationDisabled) {
            Animation a = new Animation() {
            };
            a.setDuration(0);
            return a;
        }

        Animation animation = super.onCreateAnimation(transit, enter, nextAnim);

        if (mRoot == null || !mRoot.isHardwareAccelerated()) {
            return animation;
        }

        if (animation == null && nextAnim != 0) {
            animation = AnimationUtils.loadAnimation(getActivity(), nextAnim);
        }

        if (animation != null) {
            mRoot.setLayerType(View.LAYER_TYPE_HARDWARE, null);

            animation.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {
                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    mRoot.setLayerType(View.LAYER_TYPE_NONE, null);
                }

                @Override
                public void onAnimationRepeat(Animation animation) {
                }
            });
        }
        return animation;
    }

    /*** Task Management */

    protected void sendRequest(WebService webService, String... values) {
        sendRequest(webService, this, values);
    }

    protected void sendRequest(WebService webService, OnResponseReady onResponseReady, String... values) {
        sendRequest(webService, onResponseReady, RequestMode.LIVE_ONLY, values);
    }

    protected void sendRequest(WebService webService, OnResponseReady onResponseReady,
                               RequestMode requestMode, String... values) {
        mRequestManager.builder()
                .webService(webService)
                .callback(onResponseReady)
                .requestMode(requestMode)
                .values(values)
                .send();
    }

    @Override
    public void onDialogPositive() {
        mMessageDialog.dismiss();
    }

    @Override
    public void onDialogNegative() {
        mMessageDialog.dismiss();
    }

    private void calcToolbarHeight() {
        TypedValue typedValue = new TypedValue();
        int resId = android.support.v7.appcompat.R.attr.actionBarSize;
        if (getActivity() != null && getActivity().getTheme().resolveAttribute(resId, typedValue, true)) {
            mToolbarHeight = TypedValue.complexToDimensionPixelSize(typedValue.data, getResources().getDisplayMetrics());
        }
    }

    protected void setLoadingUtilsState(LoadingUtils.State state) {
        if (mLoadingUtils != null) {
            mLoadingUtils.setState(state);
        }
    }

    /*** Fragment navigation */

    protected void moveToFragment(Fragment fragment) {
        moveToFragment(fragment, R.id.container, true);
    }

    protected void moveToFragment(Fragment fragment, int containerId, boolean addToBackStack) {
        if (getActivity() == null) return;
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.slide_enter, R.anim.slide_leave,
                R.anim.slide_enter, R.anim.slide_leave);
        transaction.replace(containerId, fragment, null);
        if (addToBackStack) transaction.addToBackStack(null);
        transaction.commit();
    }

    protected void moveToFragmentDelayed(Fragment fragment) {
        moveToFragmentDelayed(fragment, R.id.container, true, DELAY_FRAGMENT_NAVIGATION);
    }

    protected void moveToFragmentDelayed(final Fragment fragment, final int containerId,
                                         final boolean addToBackStack, long delay) {
        if (mIsNavigating) {
            return;
        }
        mIsNavigating = true;
        mDelayHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                mIsNavigating = false;
                moveToFragment(fragment, containerId, addToBackStack);
            }
        }, delay);
    }

    @Override
    public void onDestroyView() {
        mRequestManager.cancelTasks();
        mDelayHandler.removeCallbacks(null);
        if (mLoadingUtils != null) {
            mLoadingUtils.removeLoading();
        }
        mIsSelected = false;
        mViewCreated = false;
        mHasBeenSelected = false;
        super.onDestroyView();
    }
}
package com.hamzeh.easydriod.controller.utils;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Environment;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.media.ExifInterface;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.FileProvider;
import android.util.Log;

import org.hamzeh.easydroid.R;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

public class EasydroidMediaPicker {
    private static final String TAG = EasydroidMediaPicker.class.getSimpleName();

    public static final String EXT = "jpg";
    public static final String SUFFIX = ".jpg";
    public static final int DEFAULT_WIDTH = 1080;
    public static final int REQUEST_CODE_MEDIA_PICKER = 10012;

    private Uri photoURI;
    private String mCurrentPhotoPath;
    private String mFileId;
    private MediaPickedCallback mMediaPickedCallback;

    private int mRequestCode;

    private static EasydroidMediaPicker sEasydroidMediaPicker;

    public static EasydroidMediaPicker getInstance() {
        if (sEasydroidMediaPicker == null) {
            sEasydroidMediaPicker = new EasydroidMediaPicker();
        }
        return sEasydroidMediaPicker;
    }

    public void startPickingImage(Context context, MediaPickedCallback mediaPickedCallback) {
        startPickingImage(context, REQUEST_CODE_MEDIA_PICKER, mediaPickedCallback);
    }

    public void startPickingImage(Context context, int requestCode, MediaPickedCallback callback) {
        mRequestCode = requestCode;
        mMediaPickedCallback = callback;
        Activity activity = ((Activity) context);
        if (activity == null || activity.isDestroyed() || activity.isFinishing()) {
            return;
        }
        checkPermission(activity);
    }

    private void checkPermission(final Activity activity) {
        EasydroidPermissions.getInstance().checkPermissions(activity,
                new EasydroidPermissions.GrantPermissionCallback() {
                    @Override
                    public void onGrantPermissionsResult(int requestCode, boolean grantedAll, List<String> grantedPermissions) {
                        if (grantedAll) {
                            postPermissionCheck(activity);
                        }
                    }
                }, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA);
    }

    private void postPermissionCheck(Activity activity) {
        mFileId = String.format("%10s", String.valueOf(Short.MAX_VALUE))
                .replace(' ', '0');
        Intent chooserIntent = getChooserIntent(activity);
        activity.startActivityForResult(chooserIntent, mRequestCode);
    }

    public Intent getChooserIntent(Context context) {
        Intent chooserIntent = null;
        List<Intent> intentList = new ArrayList<>();
        Intent pickIntent = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intentList = addIntentsToList(context, intentList, pickIntent);
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(context.getPackageManager()) != null) {
            File photoFile = null;
            try {
                photoFile = createImageFile(context);
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            if (photoFile != null) {
                photoURI = FileProvider.getUriForFile(context, context.getPackageName(), photoFile);
                takePictureIntent.putExtra("return-data", true);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                intentList = addIntentsToList(context, intentList, takePictureIntent);
            }
        }
        if (intentList.size() > 0) {
            chooserIntent = Intent.createChooser(intentList.remove(intentList.size() - 1),
                    context.getString(R.string.label_pick_image));
            chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, intentList.toArray(new Parcelable[]{}));
        }
        return chooserIntent;
    }

    private List<Intent> addIntentsToList(Context ctx, List<Intent> list, Intent intent) {
        List<ResolveInfo> resInfo = ctx.getPackageManager().queryIntentActivities(intent, 0);
        for (ResolveInfo resolveInfo : resInfo) {
            String packageName = resolveInfo.activityInfo.packageName;
            Intent targetedIntent = new Intent(intent);
            targetedIntent.setPackage(packageName);
            list.add(targetedIntent);
            Log.d(TAG, "Intent: " + intent.getAction() + " package: " + packageName);
        }
        return list;
    }

    private File createImageFile(Context context) throws IOException {
        File storageDir = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(mFileId, SUFFIX, storageDir);
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    public Bitmap getDecodedBitmap(int w, int h) {
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;
        int scaleFactor = Math.min(photoW / w, photoH / h);
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inPurgeable = true;
        return BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
    }

    public void addToGallery(Context context) {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        File f = new File(mCurrentPhotoPath);
        Uri contentUri = Uri.fromFile(f);
        mediaScanIntent.setData(contentUri);
        context.sendBroadcast(mediaScanIntent);
    }

    public String getGalleryImageAdaptedPath(Context context, String path) {
        Uri uri = Uri.parse(path);
        String[] data = {MediaStore.Images.Media.DATA};
        CursorLoader loader = new CursorLoader(context, uri, data,
                null, null, null);
        Cursor cursor = loader.loadInBackground();
        if (cursor == null) {
            return path;
        }
        int columnIndex = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        if (cursor.moveToFirst()) {
            String result = cursor.getString(columnIndex);
            cursor.close();
            return result;
        }
        return path;
    }

    public String getCurrentPhotoPath() {
        return mCurrentPhotoPath;
    }

    public void setCurrentPhotoPath(String mCurrentPhotoPath) {
        this.mCurrentPhotoPath = mCurrentPhotoPath;
    }

    public Uri getPhotoURI() {
        return photoURI;
    }

    public void setPhotoURI(Uri photoURI) {
        this.photoURI = photoURI;
    }

    /*** Resize */

    public String resizeFile(Context context, int targetWidth) {
        Bitmap bMap = BitmapFactory.decodeFile(mCurrentPhotoPath);
        int width = bMap.getWidth();
        int height = bMap.getHeight();
        float scale = ((float) targetWidth) / width;
        if (scale > 1) scale = 1;
        Matrix matrix = new Matrix();
        matrix.postScale(scale, scale);
        Bitmap resizedBitmap = Bitmap.createBitmap(
                bMap, 0, 0, width, height, matrix, false);
        String resizedFile = saveNewBitmap(context, resizedBitmap);
        bMap.recycle();
        return resizedFile;
    }

    @Nullable
    public String saveNewBitmap(Context context, Bitmap out) {
        try {
            File storageDir = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
            File resizedFile = File.createTempFile(String.valueOf(Short.MAX_VALUE),
                    EasydroidMediaPicker.SUFFIX, storageDir);
            OutputStream fOut = new BufferedOutputStream(new FileOutputStream(resizedFile));
            out.compress(Bitmap.CompressFormat.JPEG, 100, fOut);
            fOut.flush();
            fOut.close();
            out.recycle();
            return resizedFile.getPath();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Fixing Rotation
     */

    public void adaptRotationAndSize(Context context, boolean isCamera) {
        int rotation = getRotation(context, isCamera);
        Bitmap bm = null, rotatedBitmap = null;
        if (rotation != 0) {
            Matrix matrix = new Matrix();
            matrix.postRotate(rotation);
            try {
                bm = MediaStore.Images.Media.getBitmap(context.getContentResolver(), photoURI);
                rotatedBitmap = Bitmap.createBitmap(bm, 0, 0,
                        bm.getWidth(), bm.getHeight(), matrix, true);
                File photoFile = createImageFile(context);
                photoURI = FileProvider.getUriForFile(context, context.getPackageName(), photoFile);
                FileOutputStream out = new FileOutputStream(photoFile);
                rotatedBitmap.compress(Bitmap.CompressFormat.JPEG, 70, out);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (bm != null) bm.recycle();
                if (rotatedBitmap != null) rotatedBitmap.recycle();
            }
        } else {
            try {
                bm = MediaStore.Images.Media.getBitmap(context.getContentResolver(), photoURI);
                File photoFile = createImageFile(context);
                photoURI = FileProvider.getUriForFile(context, context.getPackageName(), photoFile);
                FileOutputStream out = new FileOutputStream(photoFile);
                bm.compress(Bitmap.CompressFormat.JPEG, 70, out);
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (bm != null) bm.recycle();
            }
        }
    }

    private int getRotationFromCamera(Context context) {
        int rotate = 0;
        try {
            context.getContentResolver().notifyChange(photoURI, null);
            ExifInterface exif = new ExifInterface(mCurrentPhotoPath);
            int orientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_NORMAL);
            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_270: {
                    rotate = 270;
                    break;
                }
                case ExifInterface.ORIENTATION_ROTATE_180: {
                    rotate = 180;
                    break;
                }
                case ExifInterface.ORIENTATION_ROTATE_90: {
                    rotate = 90;
                    break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return rotate;
    }

    private int getRotationFromGallery(Context context) {
        int result = 0;
        String[] columns = {MediaStore.Images.Media.ORIENTATION};
        Cursor cursor = null;
        try {
            cursor = context.getContentResolver().query(photoURI, columns,
                    null, null, null);
            if (cursor != null && cursor.moveToFirst()) {
                result = cursor.getInt(cursor.getColumnIndex(MediaStore.Images.Media.ORIENTATION));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (cursor != null) {
            cursor.close();
        }
        return result;
    }

    private int getRotation(Context context, boolean isCamera) {
        int rotation;
        if (isCamera) {
            rotation = getRotationFromCamera(context);
        } else {
            rotation = getRotationFromGallery(context);
        }
        Log.d(TAG, "Image rotation: " + rotation);
        return rotation;
    }

    public void onActivityResult(Context context, int requestCode, int resultCode, Intent data) {
        if (resultCode != Activity.RESULT_OK || requestCode != mRequestCode) {
            return;
        }
        boolean isCamera = (data == null ||
                data.getData() == null ||
                data.getData().toString().contains(getCurrentPhotoPath()));
        if (!isCamera) {
            setCurrentPhotoPath(getGalleryImageAdaptedPath(context, data.getData().toString()));
            setPhotoURI(Uri.parse(data.getData().toString()));
        }
        Log.i(TAG, "Path :- Picked " + getCurrentPhotoPath());
        Log.i(TAG, "Path :- Uri " + getPhotoURI().toString());
        adaptRotationAndSize(context, isCamera);
        mMediaPickedCallback.onMediaPicked(requestCode, getCurrentPhotoPath());
    }

    public int getRequestCode() {
        return mRequestCode;
    }

    public interface MediaPickedCallback {
        void onMediaPicked(int requestCode, String imagePath);
    }
}
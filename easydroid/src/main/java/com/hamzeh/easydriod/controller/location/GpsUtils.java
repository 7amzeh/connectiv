package com.hamzeh.easydriod.controller.location;

import android.content.Context;
import android.os.Build;
import android.provider.Settings;

public class GpsUtils {
    public static boolean isGpsEnabled(Context ctx) {
        if (ctx == null) return false;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            try {
                int locationMode = Settings.Secure.getInt(ctx.getContentResolver(),
                        Settings.Secure.LOCATION_MODE);
                return locationMode != Settings.Secure.LOCATION_MODE_OFF;
            } catch (Settings.SettingNotFoundException e) {
                e.printStackTrace();
                return false;
            }
        } else {
            String locationProviders = Settings.Secure.getString(ctx.getContentResolver(),
                    Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
            return !locationProviders.isEmpty();
        }
    }
}

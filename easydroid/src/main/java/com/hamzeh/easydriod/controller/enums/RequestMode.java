package com.hamzeh.easydriod.controller.enums;

public enum RequestMode {
    LIVE_ONLY,
    CACHE_ONLY,
    CACHE_THEN_LIVE,
    LIVE_THEN_CACHE;

    public boolean shouldGetCacheInitially() {
        return this == CACHE_ONLY || this == CACHE_THEN_LIVE;
    }

    public boolean shouldGetCacheFinally() {
        return this == LIVE_THEN_CACHE;
    }
}
package com.connectiv.hamzehtask.model.machines;

import com.google.gson.annotations.SerializedName;

public class CommunicationProtocol {
    @SerializedName("id")
    public long id;
    @SerializedName("name")
    public String name;
    @SerializedName("defaultPort")
    public long defaultPort;
}
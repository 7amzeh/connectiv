package com.connectiv.hamzehtask.model;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.connectiv.hamzehtask.model.machines.MachinesResponse;
import com.hamzeh.easydriod.model.server.WebService;

import java.io.IOException;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import okhttp3.Authenticator;
import okhttp3.Credentials;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.Route;

import static com.hamzeh.easydriod.model.ObscureSharedPreferences.TAG;

public class WebServices {
    public static String BASE_URL = "https://45.55.43.15:9090/api/";
    public static String WHITELISTED_IP = "45.55.43.15";

    //TODO: warning, testing credentials, to be removed before releasing
    public static String USERNAME = "admin@boot.com";
    public static String PASSWORD = "admin";

    public static final WebService MACHINES = WebService.builder()
            .get(BASE_URL)
            .api("machine")
            .params("page", "size")
            .responseClass(MachinesResponse.class)
            .build();

    public static OkHttpClient getSelfSignedClient() {
        TrustManager[] trustManagers = new TrustManager[]{new X509TrustManager() {
            @Override
            public X509Certificate[] getAcceptedIssuers() {
                return new X509Certificate[0];
            }

            @Override
            public void checkServerTrusted(final X509Certificate[] chain,
                                           final String authType) {
            }

            @Override
            public void checkClientTrusted(final X509Certificate[] chain,
                                           final String authType) {
            }
        }};

        X509TrustManager x509TrustManager = new X509TrustManager() {
            @Override
            public X509Certificate[] getAcceptedIssuers() {
                return new X509Certificate[0];
            }

            @Override
            public void checkServerTrusted(final X509Certificate[] chain, final String authType) {
            }

            @Override
            public void checkClientTrusted(final X509Certificate[] chain, final String authType) {
            }
        };

        OkHttpClient.Builder okHttpClientBuilder = new OkHttpClient.Builder();
        okHttpClientBuilder.connectTimeout(10, TimeUnit.SECONDS)
                .writeTimeout(10, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS);

        try {
            final String PROTOCOL = "SSL";
            SSLContext sslContext = SSLContext.getInstance(PROTOCOL);
            SecureRandom secureRandom = new SecureRandom();
            sslContext.init(null, trustManagers, secureRandom);
            SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();
            okHttpClientBuilder.sslSocketFactory(sslSocketFactory, x509TrustManager);
        } catch (Exception e) {
            e.printStackTrace();
        }

        okHttpClientBuilder.hostnameVerifier(new HostnameVerifier() {
            @Override
            public boolean verify(String hostname, SSLSession session) {
                return WHITELISTED_IP.equals(hostname);
            }
        });

        okHttpClientBuilder.authenticator(new Authenticator() {
            @Nullable
            @Override
            public Request authenticate(@NonNull Route route, @NonNull Response response) throws IOException {
                String credential = Credentials.basic(USERNAME, PASSWORD);
                return response.request().newBuilder().header("Authorization", credential).build();
            }
        });

        return okHttpClientBuilder.build();
    }
}
package com.connectiv.hamzehtask.model.machines;

import com.google.gson.annotations.SerializedName;

public class Model {
    @SerializedName("id")
    public long id;
    @SerializedName("name")
    public String name;
    @SerializedName("creationDate")
    public Object creationDate;
    @SerializedName("expiryDate")
    public Object expiryDate;
}
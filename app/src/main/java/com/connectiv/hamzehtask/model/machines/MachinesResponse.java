package com.connectiv.hamzehtask.model.machines;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class MachinesResponse {
    @SerializedName("content")
    public List<Machine> content = new ArrayList<>();
    @SerializedName("last")
    public boolean last;
    @SerializedName("totalPages")
    public long totalPages;
    @SerializedName("totalElements")
    public long totalElements;
    @SerializedName("numberOfElements")
    public long numberOfElements;
    @SerializedName("sort")
    public Object sort;
    @SerializedName("first")
    public boolean first;
    @SerializedName("size")
    public long size;
    @SerializedName("number")
    public long number;
}
package com.connectiv.hamzehtask.model.machines;

import com.google.gson.annotations.SerializedName;

public class TargetMachine {
    @SerializedName("id")
    public long id;
    @SerializedName("sourceMachineId")
    public long sourceMachineId;
    @SerializedName("targetMachine")
    public TargetMachine targetMachine;
    @SerializedName("circuitStatusId")
    public long circuitStatusId;
}
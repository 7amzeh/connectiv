
package com.connectiv.hamzehtask.model.machines;

import com.google.gson.annotations.SerializedName;

public class Type {
    @SerializedName("id")
    public long id;
    @SerializedName("name")
    public String name;
}
package com.connectiv.hamzehtask.model.machines;

import com.connectiv.hamzehtask.view.ListItems;
import com.google.gson.annotations.SerializedName;
import com.hamzeh.easydriod.view.items.ListItem;
import com.hamzeh.easydriod.view.items.ListItemType;

import java.util.ArrayList;
import java.util.List;

public class Machine extends ListItem {
    @SerializedName("id")
    public long id;
    @SerializedName("name")
    public String name;
    @SerializedName("ipAddress")
    public String ipAddress;
    @SerializedName("ipSubnetMask")
    public String ipSubnetMask;
    @SerializedName("model")
    public Model model;
    @SerializedName("locationId")
    public long locationId;
    @SerializedName("status")
    public Status status;
    @SerializedName("type")
    public Type type;
    @SerializedName("serialNumber")
    public String serialNumber;
    @SerializedName("version")
    public String version;
    @SerializedName("communicationProtocols")
    public List<CommunicationProtocol> communicationProtocols = new ArrayList<>();
    @SerializedName("targetMachines")
    public List<TargetMachine> targetMachines = new ArrayList<>();
    @SerializedName("location")
    public long location;
    @SerializedName("serialNum")
    public String serialNum;

    @Override
    public ListItemType getListItemType() {
        return ListItems.MACHINE;
    }

    @Override
    public long getStableId() {
        return id;
    }

    @Override
    public int hashCode() {
        return (int) (id ^ (id >>> 32));
    }

    @Override
    public boolean equals(Object other) {
        return other == this || other instanceof Machine && id == ((Machine) other).id;
    }
}
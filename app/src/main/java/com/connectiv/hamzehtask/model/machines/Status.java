package com.connectiv.hamzehtask.model.machines;

import com.google.gson.annotations.SerializedName;

public class Status {
    @SerializedName("id")
    public int id;
    @SerializedName("statusValue")
    public String statusValue;
    @SerializedName("legacyValue")
    public String legacyValue;
}
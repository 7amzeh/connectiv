package com.connectiv.hamzehtask.controller;

import com.connectiv.hamzehtask.R;

public enum MachineStatus {
    OTHER(0, R.drawable.circle_status_blue),
    UNCHECKED(1, R.drawable.circle_status_green),
    CHECKING(2, R.drawable.circle_status_orange),
    UP(3, R.drawable.circle_status_yellow),
    DOWN(4, R.drawable.circle_status_red);

    public int id;
    public int iconResId;

    MachineStatus(int id, int iconResId) {
        this.id = id;
        this.iconResId = iconResId;
    }

    public static MachineStatus findById(int statusId) {
        for (MachineStatus status : MachineStatus.values()) {
            if (status.id == statusId) {
                return status;
            }
        }
        return MachineStatus.OTHER;
    }
}

package com.connectiv.hamzehtask.controller;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.connectiv.hamzehtask.R;
import com.hamzeh.easydriod.controller.activities.EasydroidActivity;
import com.hamzeh.easydriod.controller.activities.EasydroidFragmentActivity;
import com.hamzeh.easydriod.controller.interfaces.OnResponseReady;
import com.hamzeh.easydriod.model.server.Response;
import com.hamzeh.easydriod.view.ViewVisibilityHandler;

public class SplashScreenActivity extends EasydroidActivity {
    private static final long TIME_TO_LEAVE = 2000;

    private Handler mLeaveHandler;
    private boolean mLeaveOnStart;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        mLeaveHandler = new Handler();
        animateLogo();
        leave();
    }

    private void animateLogo() {
        Animation enterAnim = AnimationUtils.loadAnimation(mContext, R.anim.view_enter);
        enterAnim.setDuration(TIME_TO_LEAVE / 2);
        findViewById(R.id.img_logo).startAnimation(enterAnim);
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (mLeaveOnStart) {
            mLeaveOnStart = false;
            leave();
        }
    }

    private void leave() {
        mLeaveHandler.postDelayed(mLeaveRunnable, TIME_TO_LEAVE);
    }

    private Runnable mLeaveRunnable = new Runnable() {
        @Override
        public void run() {
            Intent intent = new Intent(mContext, MainActivity.class);
            intent.putExtra(MainActivity.FRAGMENT_NAME, MainFragment.class.getName());
            startActivity(intent);
            finish();
        }
    };

    @Override
    protected void onStop() {
        mLeaveOnStart = true;
        mLeaveHandler.removeCallbacksAndMessages(null);
        super.onStop();
    }
}
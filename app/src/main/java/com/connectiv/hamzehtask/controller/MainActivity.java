package com.connectiv.hamzehtask.controller;

import android.os.Bundle;

import com.connectiv.hamzehtask.R;
import com.hamzeh.easydriod.controller.activities.EasydroidFragmentActivity;

public class MainActivity extends EasydroidFragmentActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_main;
    }
}

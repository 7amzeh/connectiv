package com.connectiv.hamzehtask.controller;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.connectiv.hamzehtask.R;
import com.connectiv.hamzehtask.model.WebServices;
import com.connectiv.hamzehtask.model.machines.Machine;
import com.connectiv.hamzehtask.model.machines.MachinesResponse;
import com.connectiv.hamzehtask.view.ToolbarManager;
import com.connectiv.hamzehtask.view.SpaceItemDecoration;
import com.hamzeh.easydriod.controller.fragments.EasydroidListFragment;
import com.hamzeh.easydriod.model.server.Response;
import com.hamzeh.easydriod.model.server.WebService;
import com.hamzeh.easydriod.view.items.ListItem;
import com.hamzeh.easydriod.view.items.LoadingMore;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class MainFragment extends EasydroidListFragment {
    public static final int DEFAULT_PAGE_SIZE = 10;

    private ToolbarManager mToolbarManager;
    private String mFiltrationQuery = "";
    private MachineStatus mFilteredStatus;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_main, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mToolbarManager = new ToolbarManager((Toolbar) findViewById(R.id.toolbar));
        mToolbarManager.setCallback(mToolbarCallback);
    }

    @Override
    protected WebService getWebService() {
        return WebServices.MACHINES;
    }

    @Override
    protected void requestItems() {
        sendRequest(getWebService(), String.valueOf(mListOptions.getPageNum()),
                String.valueOf(DEFAULT_PAGE_SIZE));
    }

    @Override
    protected void onListSuccess(Response response) {
        super.onListSuccess(response);
        MachinesResponse result = (MachinesResponse) response.result;
        mToolbarManager.setTotalCount(result.totalElements);
    }

    @Override
    protected List<ListItem> getItems(Response response) {
        MachinesResponse result = (MachinesResponse) response.result;
        return new ArrayList<ListItem>(result.content);
    }

    @Override
    protected List<ListItem> combinePages() {
        List<ListItem> filteredItems = new ArrayList<>();
        List<ListItem> allItems = super.combinePages();
        for (ListItem listItem : allItems) {
            if (listItem instanceof Machine) {
                Machine machine = (Machine) listItem;
                String adaptedName = machine.name.toLowerCase(Locale.ENGLISH);
                boolean nameMatches = adaptedName.contains(mFiltrationQuery);
                boolean statusMatches = mFilteredStatus == null || mFilteredStatus.id == machine.status.id;
                if (nameMatches && statusMatches) {
                    filteredItems.add(listItem);
                }
            }
        }
        return filteredItems;
    }

    @Override
    protected boolean hasNext(Response response) {
        MachinesResponse result = (MachinesResponse) response.result;
        return !result.last;
    }

    @Override
    protected void addItemDecoration() {
        int spacing = getResources().getDimensionPixelOffset(R.dimen.margin4);
        SpaceItemDecoration decoration = new SpaceItemDecoration(spacing);
        mRecyclerView.addItemDecoration(decoration);
    }

    @Override
    public void onItemClicked(View view, ListItem listItem, int position) {
        super.onItemClicked(view, listItem, position);
        mEasydroidAdapter.setSingleSelection(position);
    }

    private ToolbarManager.Callback mToolbarCallback = new ToolbarManager.Callback() {
        @Override
        public void onTextFiltered(String query) {
            mFiltrationQuery = query.toLowerCase(Locale.ENGLISH);
            performFiltration();
        }

        @Override
        public void onPerformSearch(String query) {
        }

        @Override
        public void onStatusFiltered(MachineStatus status) {
            mFilteredStatus = status;
            performFiltration();
        }
    };

    private void performFiltration() {
        List<ListItem> filteredItems = combinePages();
        if (mFilteredStatus == null && mFiltrationQuery.isEmpty()) {
            filteredItems.add(new LoadingMore(MainFragment.this));
        }
        mEasydroidAdapter.replaceContents(filteredItems, true);
    }
}

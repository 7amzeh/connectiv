package com.connectiv.hamzehtask.controller;

import android.app.Application;

import com.connectiv.hamzehtask.model.WebServices;
import com.hamzeh.easydriod.model.server.EasydroidHttp;

import java.util.Map;

import okhttp3.OkHttpClient;

public class ConnectivApp extends Application {
    public static final String TAG = Application.class.getSimpleName();

    @Override
    public void onCreate() {
        super.onCreate();

        EasydroidHttp.initWith(new EasydroidHttp.EasydroidInitializer() {
            @Override
            public Map<String, String> getDefaultHeaders() {
                return null;
            }

            @Override
            public OkHttpClient getOkHttpClient() {
                return WebServices.getSelfSignedClient();
            }

            @Override
            public String getJwtKey() {
                return null;
            }

            @Override
            public String getJwtParamName() {
                return null;
            }
        });
    }
}

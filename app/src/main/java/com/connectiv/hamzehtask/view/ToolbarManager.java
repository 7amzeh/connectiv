package com.connectiv.hamzehtask.view;

import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import com.connectiv.hamzehtask.R;
import com.connectiv.hamzehtask.controller.MachineStatus;

public class ToolbarManager {
    private Toolbar mToolbar;

    private EditText mSearchEditText;

    private TextView mStatusAllTextView;
    private TextView mStatusActiveTextView;
    private TextView mStatusDownTextView;

    private TextView mTotalCountTextView;

    private Callback mCallback;

    public ToolbarManager(Toolbar toolbar) {
        mToolbar = toolbar;
        initSearch();
        initStatuses();
    }

    private void initStatuses() {
        mStatusAllTextView = mToolbar.findViewById(R.id.txt_status_all);
        mStatusActiveTextView = mToolbar.findViewById(R.id.txt_status_active);
        mStatusDownTextView = mToolbar.findViewById(R.id.txt_status_down);

        mTotalCountTextView = mToolbar.findViewById(R.id.txt_total_elements);

        mStatusAllTextView.setOnClickListener(mOnStatusClickListener);
        mStatusActiveTextView.setOnClickListener(mOnStatusClickListener);
        mStatusDownTextView.setOnClickListener(mOnStatusClickListener);

        mStatusAllTextView.setSelected(true);
    }

    private View.OnClickListener mOnStatusClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            mStatusAllTextView.setSelected(mStatusAllTextView.getId() == v.getId());
            mStatusActiveTextView.setSelected(mStatusActiveTextView.getId() == v.getId());
            mStatusDownTextView.setSelected(mStatusDownTextView.getId() == v.getId());

            MachineStatus status = null;
            if (v.getId() == R.id.txt_status_active) {
                status = MachineStatus.UP;
            } else if (v.getId() == R.id.txt_status_down) {
                status = MachineStatus.DOWN;
            }
            if (mCallback != null) {
                mCallback.onStatusFiltered(status);
            }
        }
    };

    private void initSearch() {
        mSearchEditText = mToolbar.findViewById(R.id.etxt_search);
        mSearchEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH && mCallback != null) {
                    mCallback.onPerformSearch(mSearchEditText.getText().toString());
                    return true;
                }
                return false;
            }
        });
        mSearchEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (mCallback != null) {
                    mCallback.onTextFiltered(s.toString());
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }

    public void setCallback(Callback callback) {
        mCallback = callback;
    }

    public void setTotalCount(long totalElements) {
        mTotalCountTextView.setText(String.valueOf(totalElements));
    }

    public interface Callback {
        void onTextFiltered(String query);

        void onPerformSearch(String query);

        void onStatusFiltered(MachineStatus status);
    }
}

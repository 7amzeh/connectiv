package com.connectiv.hamzehtask.view;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.connectiv.hamzehtask.R;
import com.connectiv.hamzehtask.controller.MachineStatus;
import com.connectiv.hamzehtask.model.machines.Machine;
import com.hamzeh.easydriod.controller.interfaces.ListItemCallback;
import com.hamzeh.easydriod.view.items.ListItem;
import com.hamzeh.easydriod.view.viewholders.EasydroidViewHolder;

public class MachineViewHolder extends EasydroidViewHolder {
    private LinearLayout mRootLayout;

    private TextView mNameTextView;
    private TextView mSerialNumberTextView;

    private TextView mIpAddressTextView;
    private TextView mMaskTextView;

    private ImageView mStatusImageView;

    private View mSeparatorView;

    public MachineViewHolder(View itemView, ListItemCallback callback) {
        super(itemView, callback);
        mRootLayout = findViewById(R.id.layout_root);

        mNameTextView = findViewById(R.id.txt_machine_name);
        mSerialNumberTextView = findViewById(R.id.txt_serial_number);
        mIpAddressTextView = findViewById(R.id.txt_ip_address);

        mMaskTextView = findViewById(R.id.txt_mask);
        mStatusImageView = findViewById(R.id.img_status);
        mSeparatorView = findViewById(R.id.sep);

        attachClickListener(itemView);
    }

    @Override
    public void draw(ListItem listItem) {
        super.draw(listItem);
        Machine machine = (Machine) listItem;
        mNameTextView.setText(adapt(machine.name));
        mSerialNumberTextView.setText(adapt(machine.serialNumber));
        mIpAddressTextView.setText(adapt(machine.ipAddress));
        mMaskTextView.setText(adapt(machine.ipSubnetMask));

        MachineStatus machineStatus = MachineStatus.findById(machine.status.id);
        mStatusImageView.setBackgroundResource(machineStatus.iconResId);
        if (machine.isSelected()) {
            mRootLayout.setBackgroundResource(R.color.blue_selection);
            mSeparatorView.setVisibility(View.INVISIBLE);
        } else {
            mRootLayout.setBackgroundResource(android.R.color.transparent);
            mSeparatorView.setVisibility(View.VISIBLE);
        }
    }
}
